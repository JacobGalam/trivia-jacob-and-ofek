# Trivia

Trivia game, Magshimim project written in WPF (C#) and C++ socket and SQL for the server side. Made by Jacob Galam and Ofek Haimovich.

## Features

- Login and register
- Users made questions
- Rooms and private rooms
- Kick and ban user from room
- Live game scoreboard
- Realtime game chat and room chat
- Categories for questions 
- Custom made sounds and music
- And much more! 

![](https://media.giphy.com/media/IbmQjQkVsj7ZZeawZ0/giphy.gif)

![img](https://imgur.com/6ghkTJ4.png)
![img](https://imgur.com/OugeJGY.png)
![img](https://imgur.com/51bBKBD.png)

## Installation

For initial the DBs open CMD in the project folder.

```bash
cd TriviaBack
python init_trivias.py
```

After that build and run the server file (TriviaBack project) and build and run appliction (TriviaFront project)