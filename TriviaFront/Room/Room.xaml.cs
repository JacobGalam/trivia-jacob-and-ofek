﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TriviaFront.Classes.Networking;
using TriviaFront.Game;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;
using ControlzEx.Theming;
using MahApps.Metro.Controls;

namespace TriviaFront.Room
{
    /// <summary>
    /// Interaction logic for Room.xaml
    /// </summary>
    public partial class Room : MetroWindow
    {
        private Chat.Chat chat;
        private string _selectPlayer;
        private bool isRoomOwner;

        public Room(bool isRoomOwner, Requests.CreateRoom roomData, List<string> players)
        {
            InitializeComponent();
            this.isRoomOwner = isRoomOwner;

            chat = new Chat.Chat();
            chat.Show();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");

            if (!isRoomOwner)
            {
                Start.Visibility = Visibility.Hidden;
            }
            this.RoomName.Text = roomData.roomName;
            this.PlayersMax.Text = roomData.maxUsers.ToString();
            this.QuestionNum.Text = roomData.questionCount.ToString();
            this.timePerQuestion.Text = roomData.answerTimeout.ToString();

            this.PlayersList.Items.Clear();

            Communicator communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.UserStats, UpdateUserStats);
            communicator.AddEvent(Defs.Codes.ExitRoom, onUserExit);
            communicator.AddEvent(Defs.Codes.UserLeave, UpdateLeaveUser);
            communicator.AddEvent(Defs.Codes.UserJoin, UpdateNewUser);
            communicator.AddEvent(Defs.Codes.CloseRoom, onRoomClose);
            communicator.AddEvent(Defs.Codes.StartGamedata, onGameStart);
            communicator.AddEvent(Defs.Codes.KickUser, onBeingKicked);
            communicator.AddEvent(Defs.Codes.BanUser, onBeingBan);

            bool isAdmin = true;
            foreach (var player in players)
            {
                PlayersList.Items.Add(GetPlayerBox(player, isAdmin));
                GetPlayerData(player);
                isAdmin = false;
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PopupWindow.IsOpen = true;
            }
        }

        private ListBoxItem GetPlayerBox(string name, bool isAdmin)
        {
            var box = new ListBoxItem();
            TextBlock admin = new TextBlock();
            admin.Text = name;
            if (isAdmin)
            {
                box.ToolTip = "Room Admin: ";
                admin.Foreground = Brushes.Red;
            }
            box.Content = admin;
            box.MouseDoubleClick += on_click_player;
            box.MouseLeave += on_leave_player;
            return box;
        }

        public static void GetPlayerData(string name)
        {
            var statsReq = new Requests.GetUserStats();
            statsReq.userName = name;
            Communicator communicator = Communicator.Instance;
            communicator.TryToSend(statsReq);
        }

        private void UpdateUserStats(Response response)
        {
            var stats = response.ToData<Responses.GetUserStats>();
            if (stats == null) return;
            foreach (ListBoxItem item in PlayersList.Items)
            {
                if (((TextBlock)item.Content).Text == stats.userName)
                {
                    item.ToolTip += $"Number of games: {stats.numOfGame}\n Number of right answerers: {stats.numRightAns}\n Number of total answerers: {stats.numWrongAns + stats.numRightAns}\n Average answerers time: {stats.avrageTimeToAns}";
                    break;
                }
            }
        }

        private void UpdateLeaveUser(Response response)
        {
            var stats = response.ToData<Responses.UserLeave>();
            if (stats == null) return;

            for (int i = 0; i < PlayersList.Items.Count; i++)
            {
                var item = (ListBoxItem)PlayersList.Items[i];
                if (((TextBlock)item.Content).Text == stats.username)
                {
                    PlayersList.Items.RemoveAt(i);
                    break;
                }
            }
        }

        private void UpdateNewUser(Response response)
        {
            var stats = response.ToData<Responses.UserJoin>();
            if (stats == null) return;
            PlayersList.Items.Add(GetPlayerBox(stats.username, false));
            GetPlayerData(stats.username);
        }

        private void onRoomClose(Response response)
        {
            var stats = response.ToData<Responses.CloseRoom>();
            if (stats == null) return;
            MessageBox.Show("The admin close the room", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            chat.Close();
            this.Close(); //only if you want to close the current form.
        }

        private void onBeingKicked(Response response)
        {
            MessageBox.Show("The admin kick you from the room", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            chat.Close();
            this.Close(); //only if you want to close the current form.
        }

        private void onBeingBan(Response response)
        {
            MessageBox.Show("The admin ban you from the room", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            chat.Close();
            this.Close(); //only if you want to close the current form.
        }

        private void onUserExit(Response response)
        {
            var stats = response.ToData<Responses.ExitRoom>();
            if (stats == null) return;
            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            chat.Close(); //only if you want to close the current form.
            this.Close(); //only if you want to close the current form.
        }

        private void onGameStart(Response response)
        {
            var stats = response.ToData<Responses.StartGamedata>();
            if (stats == null) return;
            var newForm = new GameWindow(stats, chat); //create your new form.
            newForm.Show(); //show the new form.
            this.Close(); //only if you want to close the current form.
        }

        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            var startRoomReq = new Requests.StartGame();
            startRoomReq.status = 1;
            Communicator communicator = Communicator.Instance;
            communicator.TryToSend(startRoomReq);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            if (AreYouSure("exit this room"))
            {
                var leaveRoomReq = new Requests.ExitRoom();
                leaveRoomReq.roomName = this.RoomName.Text;
                Communicator communicator = Communicator.Instance;
                communicator.TryToSend(leaveRoomReq);
            }
        }

        public static bool AreYouSure(string msg)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", msg, System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            return (messageBoxResult == MessageBoxResult.Yes);
        }

        private void on_leave_player(object sender, RoutedEventArgs e)
        {
            PopupWindow.StaysOpen = false;
        }

        // todo: send the kick and ban
        // todo: get the kick and ban
        // merge
        // sounds
        // music

        private void on_click_player(object sender, RoutedEventArgs e)
        {
            if (!this.isRoomOwner) return;

            Communicator communicator = Communicator.Instance;
            var listBoxItem = (ListBoxItem)sender;

            var playerName = ((TextBlock)listBoxItem.Content).Text;

            // if it the admin.
            if (playerName == communicator.UserName) return;
            this._selectPlayer = playerName;
            SelectPlayerName.Text = this._selectPlayer;
            PopupWindow.IsOpen = true;
            PopupWindow.StaysOpen = true;
        }

        private void selectPlayerCancel(object sender, RoutedEventArgs e)
        {
            PopupWindow.IsOpen = false;
        }

        private void KickPlayer(object sender, RoutedEventArgs e)
        {
            if (AreYouSure($"Are you sure you want to kick '{this._selectPlayer}' from the room"))
            {
                var kickUserReq = new Requests.KickUserReq();
                kickUserReq.kickedUser = this._selectPlayer;
                Communicator communicator = Communicator.Instance;
                communicator.TryToSend(kickUserReq);
            }
        }

        private void BanPlayer(object sender, RoutedEventArgs e)
        {
            if (AreYouSure($"Are you sure you want to ban '{this._selectPlayer}' from the room"))
            {
                var banUserReq = new Requests.BanUserReq();
                banUserReq.bannedUser = this._selectPlayer;
                Communicator communicator = Communicator.Instance;
                communicator.TryToSend(banUserReq);
            }
        }
    }
}