﻿using ControlzEx.Theming;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using TriviaFront.Classes.Networking;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;

namespace TriviaFront.Main
{
    /// <summary>
    /// Interaction logic for Top.xaml
    /// </summary>
    public partial class Top : UserControl
    {
        private List<ListBoxItem> places = new List<ListBoxItem>();
        private Dictionary<string, ListBoxItem> usersTexts = new Dictionary<string, ListBoxItem>();

        public Top()
        {
            InitializeComponent();

            places.Add(this.firstPlace);
            places.Add(this.secondPlace);
            places.Add(this.thirdPlace);
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.Tops, UpdateTops);
            communicator.AddEvent(Defs.Codes.UserStats, UpdateUserStats);

            Requests.GetTop getTops = new Requests.GetTop();
            getTops.maxUsers = 3;
            var stats = communicator.TryToSend(getTops);
        }

        private void UpdateTops(Response response)
        {
            var res = response.ToData<Responses.GetTop>();
            if (res == null) return;
            //this.first.Text = AddUser(res.tops[0]);
            //this.second.Text = AddUser(res.tops[1]);
            //this.third.Text = AddUser(res.tops[2]);
            for (int i = 0; i < res.tops.Count; i++)
            {
                places[i].Content = AddUser(res.tops[i]);
                usersTexts[res.tops[i].username] = places[i];
                Room.Room.GetPlayerData(res.tops[i].username);
            }
        }

        private void UpdateUserStats(Response response)
        {
            var stats = response.ToData<Responses.GetUserStats>();
            if (stats == null) return;
            usersTexts[stats.userName].ToolTip = $"Number of games: {stats.numOfGame}\n Number of right answerers: {stats.numRightAns}\n Number of total answerers: {stats.numWrongAns + stats.numRightAns}\n Average answerers time: {stats.avrageTimeToAns}";
        }

        private string AddUser(Responses.Score user)
        {
            return ($"{user.username}: {user.score}");
        }
    }
}