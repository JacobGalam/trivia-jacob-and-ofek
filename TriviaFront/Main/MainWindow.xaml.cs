﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TriviaFront.Classes.Networking;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;
using ControlzEx.Theming;
using MahApps.Metro.Controls;

using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Threading;
using TriviaFront.Main;

namespace TriviaFront
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");

            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.Logout, MakeLogout);
        }

        private object lastPage;

        public void Navigate(object newPage)
        {
            if (lastPage != null)
            {
                if (lastPage.GetType() == newPage.GetType()) return;
            }
            lastPage = newPage;
            this.Navigator.Navigate(newPage);
        }

        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            Navigate(new CreateRoom());
        }

        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            Navigate(new JoinRoom());
        }

        private void Statics_Click(object sender, RoutedEventArgs e)
        {
            Navigate(new Statics());
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            if (Room.Room.AreYouSure("Logout"))
            {
                Communicator communicator = Communicator.Instance;
                var signout = new Requests.Logout();
                communicator.TryToSend(signout);
            }
        }

        private void MakeLogout(Response response)
        {
            var logoutRes = response.ToData<Responses.Logout>();
            if (logoutRes == null) return;
            if (logoutRes.status == 0) { MessageBox.Show("Can't Logout", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation); }

            var newForm = new SignWindow(); //create your new form.
            newForm.Show(); //show the new form.
            this.Close(); //only if you want to close the current form.
        }

        private void Top_Click(object sender, RoutedEventArgs e)
        {
            Navigate(new Top());
        }

        private bool _allowDirectNavigation = false;
        private NavigatingCancelEventArgs _navArgs = null;
        private Duration _duration = new Duration(TimeSpan.FromSeconds(0.6));
        private double _oldHeight = 0;
        private object prePage;

        private void frame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (Content != null && !_allowDirectNavigation)
            {
                e.Cancel = true;

                _navArgs = e;

                _oldHeight = Navigator.ActualHeight;

                DoubleAnimation animation0 = new DoubleAnimation();
                animation0.From = Navigator.ActualHeight;
                animation0.To = 0;
                animation0.Duration = _duration;
                animation0.Completed += SlideCompleted;
                Navigator.BeginAnimation(HeightProperty, animation0);
            }
            _allowDirectNavigation = false;
        }

        private void SlideCompleted(object sender, EventArgs e)
        {
            _allowDirectNavigation = true;

            switch (_navArgs.NavigationMode)
            {
                case NavigationMode.New:
                    if (_navArgs.Uri == null)
                    {
                        prePage = _navArgs.Content;
                        Navigator.Navigate(_navArgs.Content);
                    }
                    else
                    {
                        Navigator.Navigate(_navArgs.Uri);
                        this.SizeToContent = SizeToContent.WidthAndHeight;
                    }
                    break;

                case NavigationMode.Back:
                    Navigator.GoBack();
                    break;

                case NavigationMode.Forward:
                    Navigator.GoForward();
                    break;

                case NavigationMode.Refresh:
                    Navigator.Refresh();
                    break;
            }

            Dispatcher.BeginInvoke(DispatcherPriority.Loaded,
                (ThreadStart)delegate ()
                {
                    DoubleAnimation animation0 = new DoubleAnimation();
                    animation0.From = 0;
                    this.SizeToContent = SizeToContent.WidthAndHeight;
                    animation0.To = this.Height;
                    animation0.Duration = _duration;
                    Navigator.BeginAnimation(HeightProperty, animation0);
                });
        }

        private void myQuestions_Click(object sender, RoutedEventArgs e)
        {
            Navigate(new myQuestions(this));
        }
    }
}