﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaFront.Classes;
using TriviaFront.Room;
using TriviaFront.Classes.Networking;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;
using ControlzEx.Theming;
using MahApps.Metro.Controls;

namespace TriviaFront
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    ///

    public class Categories
    {
        private ComboBox dropDownButton;

        public Categories(ComboBox dropDownButton)
        {
            this.dropDownButton = dropDownButton;
            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.GetAllCategories, addCategories);
            communicator.TryToSend(new Requests.getCategories());
        }

        private void addCategories(Response response)
        {
            var categoriesData = response.ToData<Responses.GetAllCategories>();
            if (categoriesData == null) return;
            foreach (var item in categoriesData.categories)
            {
                dropDownButton.Items.Add(item);
            }
        }
    }

    public partial class CreateRoom : UserControl
    {
        public CreateRoom()
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.CreateRoom, MoveToTheNewRoom);
            var cat = new Categories(this.Category);
        }

        private void MoveToTheNewRoom(Response response)
        {
            var RoomData = response.ToData<Responses.CreateRoom>();
            if (RoomData == null) return;
            var players = new List<string>();
            var communicator = Communicator.Instance;
            players.Add(communicator.UserName);
            var newForm = new Room.Room(true, RoomData.roomData, players); //create your new form.
            newForm.Show(); //show the new form.
            var win = Window.GetWindow(this);
            if (win != null) win.Close();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            var roomData = new Requests.CreateRoom();
            Communicator communicator = Communicator.Instance;

            roomData.roomName = this.name.Text;
            roomData.password = this.passwordInput.Password;
            roomData.category = (string)this.Category.SelectedItem;
            try
            {
                roomData.answerTimeout = Int32.Parse(this.time.Text);
                roomData.maxUsers = Int32.Parse(this.Players.Text);
                roomData.questionCount = Int32.Parse(this.questionNum.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show($"Please enter a number in: Time per question, Max number of players and Number of question", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            communicator.TryToSend(roomData);
        }
    }
}