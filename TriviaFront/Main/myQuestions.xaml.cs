﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ControlzEx.Theming;
using TriviaFront.Classes.Networking;
using TriviaFront.Classes.Networking.Requests;

using Requests = TriviaFront.Classes.Networking.Requests;

using Responses = TriviaFront.Classes.Networking.Responses;

namespace TriviaFront.Main
{
    internal class QuestionData
    {
        public string Question { get; set; }
        public string toolTip { get; set; }

        public QuestionData()
        {
            this.toolTip = "press to delete the question";
        }
    }

    /// <summary>
    /// Interaction logic for myQuestions.xaml
    /// </summary>
    public partial class myQuestions : UserControl
    {
        private MainWindow mainWindow;

        public myQuestions(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.GetUserQuestions, showQuestions);
            communicator.AddEvent(Defs.Codes.RemoveQuestion, RemoveQuestion);
            communicator.TryToSend(new Requests.getUserQuestions());
        }

        private void showQuestions(Response response)
        {
            var userQuestionsData = response.ToData<Responses.GetUserQuestions>();
            if (userQuestionsData == null) return;
            foreach (var question in userQuestionsData.questions)
            {
                var tempQ = new QuestionData();
                tempQ.Question = HttpUtility.HtmlDecode(question);
                tempQ.toolTip = "";
                this.questionList.Items.Add(tempQ);
            }
        }

        private void RemoveQuestion(Response response)
        {
            var userQuestionsData = response.ToData<Responses.RemoveQuestion>();
            if (userQuestionsData == null) return;

            int i = 0;
            foreach (QuestionData item in questionList.Items)
            {
                if (item.Question == userQuestionsData.question)
                {
                    questionList.Items.RemoveAt(i);
                    break;
                }
                i++;
            }
        }

        private void ListBoxItem_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var asBox = (ListBoxItem)sender;
            var roomDisplayData = (QuestionData)asBox.Content;

            if (Room.Room.AreYouSure($"Delete the question '{roomDisplayData.Question}'"))
            {
                var communicator = Communicator.Instance;
                var removeQReq = new Requests.removeQuestion();
                removeQReq.questionToRemove = HttpUtility.HtmlEncode(roomDisplayData.Question);
                communicator.TryToSend(removeQReq);
            }
        }

        private void PlayClick(object sender, MouseEventArgs e)
        {
            SoundPlayer player = new SoundPlayer(TriviaFront.Properties.Resources.button_click);
            player.Load();
            player.Play();
        }

        private void PlayHover(object sender, MouseEventArgs e)
        {
            SoundPlayer player = new SoundPlayer(TriviaFront.Properties.Resources.button_hover);
            player.Load();
            player.Play();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.Navigate(new createQuestion(mainWindow));
        }
    }
}