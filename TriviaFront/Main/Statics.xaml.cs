﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaFront.Classes.Networking;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;
using ControlzEx.Theming;
using MahApps.Metro.Controls;

namespace TriviaFront
{
    /// <summary>
    /// Interaction logic for Statics.xaml
    /// </summary>
    public partial class Statics : UserControl
    {
        public Statics()
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.UserStats, showStats);

            var getUserStats = new Requests.GetUserStats();
            getUserStats.userName = communicator.UserName;
            this.userNameText.Text = $"{communicator.UserName} stats:"; ;
            communicator.TryToSend(getUserStats);
        }

        private void showStats(Response response)
        {
            var stats = response.ToData<Responses.GetUserStats>();
            if (stats == null) return;
            this.NumOfGames.Text = stats.numOfGame.ToString();
            this.avargeResponeTime.Text = stats.avrageTimeToAns.ToString();
            this.rightAnswerers.Text = stats.numRightAns.ToString();
            this.TotalAnswerers.Text = (stats.numRightAns + stats.numWrongAns).ToString();
            this.num_of_que.Text = (stats.numOfQuestions).ToString();
        }
    }
}