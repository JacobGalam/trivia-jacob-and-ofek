﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;

using ControlzEx.Theming;
using TriviaFront.Classes.Networking;
using System.Web;

namespace TriviaFront.Main
{
    /// <summary>
    /// Interaction logic for createQuestion.xaml
    /// </summary>
    public partial class createQuestion : UserControl
    {
        private MainWindow mainWindow;

        public createQuestion(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            var a = new Categories(this.Category);
            var communicator = Communicator.Instance;
            this.Difficulty.Items.Add("easy");
            this.Difficulty.Items.Add("medium");
            this.Difficulty.Items.Add("hard");
            communicator.AddEvent(Defs.Codes.AddQuestion, AddQuestion);
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            if (Room.Room.AreYouSure("You want to go back"))
                mainWindow.Navigate(new myQuestions(mainWindow));
        }

        private void AddQuestion(Response response)
        {
            mainWindow.Navigate(new myQuestions(mainWindow));
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var communicator = Communicator.Instance;

            var trivaData = new Requests.AddQuestion();

            trivaData.question = HttpUtility.HtmlEncode(this.Question.Text);
            trivaData.rightAnswer = HttpUtility.HtmlEncode(this.RightAnswer.Text);
            trivaData.wrongAnswers.Add(HttpUtility.HtmlEncode(this.wrong1.Text));
            trivaData.wrongAnswers.Add(HttpUtility.HtmlEncode(this.wrong2.Text));
            trivaData.wrongAnswers.Add(HttpUtility.HtmlEncode(this.wrong3.Text));
            trivaData.diff = (string)this.Difficulty.SelectedItem;
            trivaData.category = (string)this.Category.SelectedItem;

            communicator.TryToSend(trivaData);
        }
    }
}