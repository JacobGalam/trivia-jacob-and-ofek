﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaFront.Classes.Networking;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;
using ControlzEx.Theming;
using System.ComponentModel;
using TriviaFront.Main;
using System.Media;

namespace TriviaFront
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : UserControl
    {
        public string showRoom;

        public JoinRoom()
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            RoomsList.Items.Clear();
            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.JoinRoom, JoinToRoom);
            communicator.AddEvent(Defs.Codes.GetPlayersInRoom, UpdateRoomPlayers);
            communicator.AddEvent(Defs.Codes.GetRooms, showRooms);

            Requests.GetRooms getRooms = new Requests.GetRooms();
            communicator.TryToSend(getRooms);
        }

        private void UpdateRoomPlayers(Response response)
        {
            var players = response.ToData<Responses.GetPlayersInRoom>();
            if (players == null) return;
            // how to get senderObject?!
            foreach (RoomDisplayData RoomButton in RoomsList.Items)
            {
                if (RoomButton.RoomName == players.roomName)
                {
                    RoomButton.toolTip = $"Category: {players.category}\n";
                    RoomButton.toolTip += "Players: ";
                    foreach (var player in players.players)
                    {
                        RoomButton.toolTip += player + "\n";
                    }
                    break;
                }
            }
        }

        public class RoomDisplayData : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public string RoomName { get; set; }
            public string Image { get; set; }
            public bool HashPass { get; set; }

            private string _toolTip;

            public string toolTip
            {
                get
                {
                    return _toolTip;
                }
                set
                {
                    _toolTip = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("toolTip"));
                    }
                }
            }

            public RoomDisplayData(Responses.RoomSendibleData data)
            {
                RoomName = data.roomName;
                toolTip = "Loading...";
                HashPass = data.hasPass;
                Image = data.hasPass ? "blocked.png" : "check-mark.png";
            }
        }

        private void showRooms(Response response)
        {
            var roomsData = response.ToData<Responses.GetRooms>();
            if (roomsData == null) return;
            if (roomsData.roomNames == null) return;
            foreach (var roomName in roomsData.roomNames)
            {
                // doto show passwords
                TextBlock textBlock = new TextBlock();
                if (roomName.hasPass)
                    textBlock.Foreground = new SolidColorBrush(Colors.Red);

                RoomsList.Items.Add(new RoomDisplayData(roomName));
            }
        }

        private void JoinToRoom(Response response)
        {
            var joinRoomData = response.ToData<Responses.JoinRoom>();
            if (joinRoomData == null) return;
            if (joinRoomData.status == 1)
            {
                var newForm = new Room.Room(false, joinRoomData.roomData, joinRoomData.players); //create your new form.
                newForm.Show(); //show the new form.
                Window.GetWindow(this).Close(); //only if you want to close the current form.
            }
            else
            {
                MessageBox.Show("Can't enter the room!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void UpdatePlayers(object sender, MouseEventArgs e)
        {
            var asBox = (ListBoxItem)sender;
            RoomDisplayData senderObject = (RoomDisplayData)asBox.Content;

            string roomName = senderObject.RoomName;
            var playersReq = new Requests.GetPlayersInRoom();
            playersReq.roomName = roomName;
            var communicator = Communicator.Instance;
            communicator.TryToSend(playersReq);
        }

        private void ListBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var asBox = (ListBoxItem)sender;
            RoomDisplayData roomDisplayData = (RoomDisplayData)asBox.Content;

            var joinRoomReq = new Requests.JoinRoom();
            joinRoomReq.roomName = roomDisplayData.RoomName;
            joinRoomReq.password = "";
            if (roomDisplayData.HashPass)
            {
                var window = new EnterPasswordWindows();
                if ((bool)window.ShowDialog() == true)
                {
                    joinRoomReq.password = window.result;
                }
                else
                {
                    return;
                }
            }

            var communicator = Communicator.Instance;
            communicator.TryToSend(joinRoomReq);
        }

        private void PlayClick(object sender, MouseEventArgs e)
        {
            SoundPlayer player = new SoundPlayer(TriviaFront.Properties.Resources.button_click);
            player.Load();
            player.Play();
        }

        private void PlayHover(object sender, MouseEventArgs e)
        {
            SoundPlayer player = new SoundPlayer(TriviaFront.Properties.Resources.button_hover);
            player.Load();
            player.Play();
        }
    }
}