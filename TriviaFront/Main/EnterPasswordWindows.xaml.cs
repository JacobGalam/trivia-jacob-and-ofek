﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using ControlzEx.Theming;
using MahApps.Metro.Controls;

namespace TriviaFront.Main
{
    /// <summary>
    /// Interaction logic for EnterPasswordWindows.xaml
    /// </summary>
    public partial class EnterPasswordWindows : MetroWindow
    {
        private string password = "";

        public string result
        {
            get { return password; }
        }

        public EnterPasswordWindows()
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
        }

        private void ok(object sender, RoutedEventArgs e)
        {
            this.password = passwordBox.Password;
            Window.GetWindow(this).DialogResult = true;
            Window.GetWindow(this).Close();
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).DialogResult = false;
            Window.GetWindow(this).Close();
        }
    }
}