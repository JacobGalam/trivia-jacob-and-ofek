﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MahApps.Metro.Controls;
using ControlzEx.Theming;
using TriviaFront.Classes.Networking;
using Responses = TriviaFront.Classes.Networking.Responses;
using Requests = TriviaFront.Classes.Networking.Requests;

namespace TriviaFront.Chat
{
    /// <summary>
    /// Interaction logic for texting.xaml
    /// </summary>
    public partial class Chat : MetroWindow
    {
        public Chat()
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");

            var communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.SendMessageChat, GetMsg);
        }

        private void GetMsg(Response response)
        {
            var messageChat = response.ToData<Responses.SendMessageChat>();
            if (messageChat == null) return;
            var msgBox = new ListBoxItem();
            msgBox.Content = formatMsg(messageChat);
            this.msglist.Items.Add(msgBox);
        }

        private static string formatMsg(Responses.SendMessageChat messageChatData)
        {
            return $"{messageChatData.user}: {messageChatData.msg}";
        }

        private void textBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            var statsReq = new Requests.SendMessageReq();
            statsReq.msg = textBox.Text;
            textBox.Text = "";
            var communicator = Communicator.Instance;
            communicator.TryToSend(statsReq);
        }
    }
}