﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaFront.Classes.Networking;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;
using MahApps.Metro.Controls;
using ControlzEx.Theming;
using Microsoft.Win32;

// TODO: Home Page, Logout, Create room, Join Room, Statistics, Communicator, logout, exit, styling

namespace TriviaFront
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SignWindow : MetroWindow
    {
        private bool isOnRegister = false;

        public SignWindow()
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            RegistryKey SoftwareKey = Registry.LocalMachine.OpenSubKey("Software", true);
            RegistryKey AppNameKey = SoftwareKey.OpenSubKey("JacobAndOfekTrivai");
            if (AppNameKey != null)
            {
                object ip = AppNameKey.GetValue("ip");
                if (ip != null) serverIp.Text = (string)ip;
            }
        }

        private void register_Click(object sender, RoutedEventArgs e)
        {
            isOnRegister = !isOnRegister;
            var temp = fromName.Text;
            fromName.Text = (string)register.Content;
            register.Content = temp;

            emailInput.Visibility = isOnRegister ? Visibility.Visible : Visibility.Hidden;
            emailText.Visibility = isOnRegister ? Visibility.Visible : Visibility.Hidden;

            name.Text = "";
            emailInput.Text = "";
            password.Password = "";
        }

        private void onError(Response response)
        {
            var logRe = response.ToData<Responses.Error>();
            if (logRe == null)
            {
                return;
            }
            MessageBox.Show(logRe.message, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            if (logRe.close)
            {
                System.Windows.Application.Current.Shutdown();
            }
        }

        private void SignUp(Response response)
        {
            var logRe = response.ToData<Responses.Signup>();
            if (logRe != null && logRe.status == 1)
            {
                var newForm = new MainWindow(); //create your new form.
                newForm.Show(); //show the new form.
                Dispatcher.Invoke(() =>
                {
                    // Code causing the exception or requires UI thread access
                    this.Close(); //only if you want to close the current form.
                });
            }
            else
            {
                MessageBox.Show("Can't register!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void logIn(Response response)
        {
            var logRe = response.ToData<Responses.Login>();
            if (logRe != null && logRe.status == 1)
            {
                var newForm = new MainWindow(); //create your new form.
                newForm.Show(); //show the new form.
                this.Close(); //only if you want to close the current form.
            }
            else
            {
                MessageBox.Show("Can't login", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void summit_Click(object sender, RoutedEventArgs e)
        {
            Communicator communicator = null;
            try
            {
                Communicator.resetConnection(this.serverIp.Text);
                communicator = Communicator.Instance;
            }
            catch (System.Net.Sockets.SocketException)
            {
                MessageBox.Show("Can't connect to server", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            RegistryKey SoftwareKey = Registry.LocalMachine.OpenSubKey("Software", true);

            RegistryKey AppNameKey = SoftwareKey.CreateSubKey("JacobAndOfekTrivai");

            AppNameKey.SetValue("ip", serverIp.Text);

            communicator.AddEvent(Defs.Codes.Login, logIn);
            communicator.AddEvent(Defs.Codes.Signup, SignUp);
            communicator.AddEvent(Defs.Codes.Error, onError);
            ;
            communicator.UserName = name.Text;
            if (isOnRegister)
            {
                var signup = new Requests.Signup();

                signup.username = name.Text;
                signup.password = password.Password;
                signup.email = emailInput.Text;
                communicator.TryToSend(signup);
            }
            else
            {
                var login = new Requests.Login();
                login.username = this.name.Text;
                login.password = password.Password;
                communicator.TryToSend(login);
            }
        }
    }
}