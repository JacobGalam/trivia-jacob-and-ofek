﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Responses = TriviaFront.Classes.Networking.Responses;
using ControlzEx.Theming;
using MahApps.Metro.Controls;

namespace TriviaFront.Game
{
    /// <summary>
    /// Interaction logic for GameEndWindow.xaml
    /// </summary>
    public partial class GameEndWindow : MetroWindow
    {
        public GameEndWindow(Responses.GetGameResults gameResults)
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");

            var SortedList = gameResults.results.OrderBy(o => o.finalResult).ToList();
            SortedList.Reverse();

            foreach (var item in SortedList)
            {
                var text = new TextBlock();
                text.Text = $"Username: {item.username} Score: {item.finalResult}";
                text.ToolTip = $"Average Answer Time: {item.averageAnswerTime} Correct Answers: {item.correctAnswerCount} Wrong Answers:{item.wrongAnswerCount}";
                this.ResultList.Items.Add(text);
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            this.Close(); //only if you want to close the current form.
        }
    }
}