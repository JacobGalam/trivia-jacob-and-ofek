﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TriviaFront.Classes.Networking;
using Requests = TriviaFront.Classes.Networking.Requests;
using Responses = TriviaFront.Classes.Networking.Responses;
using System.Windows.Threading;
using ControlzEx.Theming;
using MahApps.Metro.Controls;
using NAudio.Wave;
using System.Web;

namespace TriviaFront.Game
{
    public class LoopStream : WaveStream
    {
        private WaveStream sourceStream;

        /// <summary>
        /// Creates a new Loop stream
        /// </summary>
        /// <param name="sourceStream">The stream to read from. Note: the Read method of this stream should return 0 when it reaches the end
        /// or else we will not loop to the start again.</param>
        public LoopStream(WaveStream sourceStream)
        {
            this.sourceStream = sourceStream;
            this.EnableLooping = true;
        }

        /// <summary>
        /// Use this to turn looping on or off
        /// </summary>
        public bool EnableLooping { get; set; }

        /// <summary>
        /// Return source stream's wave format
        /// </summary>
        public override WaveFormat WaveFormat
        {
            get { return sourceStream.WaveFormat; }
        }

        /// <summary>
        /// LoopStream simply returns
        /// </summary>
        public override long Length
        {
            get { return sourceStream.Length; }
        }

        /// <summary>
        /// LoopStream simply passes on positioning to source stream
        /// </summary>
        public override long Position
        {
            get { return sourceStream.Position; }
            set { sourceStream.Position = value; }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int totalBytesRead = 0;

            while (totalBytesRead < count)
            {
                int bytesRead = sourceStream.Read(buffer, offset + totalBytesRead, count - totalBytesRead);
                if (bytesRead == 0)
                {
                    if (sourceStream.Position == 0 || !EnableLooping)
                    {
                        // something wrong with the source stream
                        break;
                    }
                    // loop
                    sourceStream.Position = 0;
                }
                totalBytesRead += bytesRead;
            }
            return totalBytesRead;
        }
    }

    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : MetroWindow
    {
        private float TimeForEachQuestion = 0;
        private float Score = 0;
        private int NumOfQuestion = 0;
        private int NowQuestion = 0;
        private List<Button> answers;

        private WaveOut win;
        private WaveOut lose;

        private WaveOut song;
        private WaveOut drums;

        private DispatcherTimer _timer;
        private TimeSpan _time;
        private Chat.Chat chat;

        private ScoreBoard scoreBoard;

        public GameWindow(Responses.StartGamedata gamedata, Chat.Chat chat)
        {
            this.chat = chat;
            scoreBoard = new ScoreBoard(gamedata.players);
            scoreBoard.Show();

            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");
            answers = new List<Button>();
            TimeForEachQuestion = gamedata.timePerquestion;
            GameName.Text = gamedata.gameName;
            NumOfQuestion = gamedata.questionNum;

            Communicator communicator = Communicator.Instance;
            communicator.AddEvent(Defs.Codes.GetGameResults, onGameFinish);
            communicator.AddEvent(Defs.Codes.updateRightAnswer, onShowRigthAns);
            communicator.AddEvent(Defs.Codes.SubmitAnswer, onSumitAns);
            communicator.AddEvent(Defs.Codes.GetQuestion, onGetQuestion);
            communicator.AddEvent(Defs.Codes.LeaveGame, onLeaveGame);

            answers.Add(this.Firstanswer);
            answers.Add(this.SecondAnswer);
            answers.Add(this.ThirdAnswer);
            answers.Add(this.FourthAnswer);

            initWave(ref lose, Properties.Resources.Epic_Fail);
            initWave(ref win, Properties.Resources.Happy_Kids);
            initLoopWave(ref song, Properties.Resources.game_song);
            initLoopWave(ref drums, Properties.Resources.Drum_roll_sound_effect);

            UpdateScore();
            UpdateNumQuestions();
        }

        private void initWave(ref WaveOut wave, System.IO.UnmanagedMemoryStream resource)
        {
            var importer = new RawSourceWaveStream(resource, new WaveFormat());
            wave = new WaveOut();
            wave.Init(importer);
        }

        private void initLoopWave(ref WaveOut wave, System.IO.UnmanagedMemoryStream resource)
        {
            var importer = new RawSourceWaveStream(resource, new WaveFormat());
            var loop = new Game.LoopStream(importer);
            wave = new WaveOut();
            wave.Init(loop);
        }

        private void UpdateNumQuestions()
        {
            this.QwestionCount.Text = $"Questions {NowQuestion}/{NumOfQuestion}";
        }

        private void SetButtonsToNormal()
        {
            foreach (var item in this.answers)
            {
                item.ClearValue(Button.BackgroundProperty);
                item.IsEnabled = true;
            }
        }

        private void UpdateScore()
        {
            this.PlayerScore.Text = $"Score: {Score}";
        }

        private void submitAnswer(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var ans = (string)button.Content;
            var submitAnswer = new Requests.SubmitAnswer();
            submitAnswer.answer = HttpUtility.HtmlEncode(ans);
            Communicator communicator = Communicator.Instance;
            communicator.TryToSend(submitAnswer);
        }

        private void onGameFinish(Response response)
        {
            var stats = response.ToData<Responses.GetGameResults>();
            if (stats == null) return;
            var newForm = new GameEndWindow(stats); //create your new form.
            newForm.Show(); //show the new form.
            chat.Close();
            scoreBoard.Close();
            this.Close(); //only if you want to close the current form.
        }

        private void onShowRigthAns(Response response)
        {
            var stats = response.ToData<Responses.updateRightAnswer>();
            if (stats == null) return;
            _timer.Stop();

            this.drums.Stop();

            this.scoreBoard.updateScoreBoard(stats.gameScores);

            if (this.Score != stats.score)
            {
                TimeLeft.Text = "YAY!";
                initWave(ref win, Properties.Resources.Happy_Kids);
                win.Play();
            }
            else
            {
                TimeLeft.Text = "OOF!";
                initWave(ref lose, Properties.Resources.Epic_Fail);
                lose.Play();
            }
            this.Score = stats.score;
            UpdateScore();

            foreach (var item in this.answers)
            {
                item.IsEnabled = true;
                if ((string)item.Content == stats.rightAnswer)
                {
                    item.Background = new SolidColorBrush(Colors.Green);
                }
                else
                {
                    item.Background = new SolidColorBrush(Colors.Red);
                }
            }
        }

        private void onSumitAns(Response response)
        {
            var stats = response.ToData<Responses.SubmitAnswer>();
            if (stats == null) return;
            if (stats.status != 1) MessageBox.Show("Can't Answer!", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            foreach (var item in this.answers)
            {
                item.IsEnabled = false;
            }
        }

        private void onGetQuestion(Response response)
        {
            var stats = response.ToData<Responses.GetQuestion>();
            if (stats == null) return;

            this.song.Play();

            this.Qwestion.Text = HttpUtility.HtmlDecode(stats.question);
            for (int i = 0; i < 4; i++)
            {
                this.answers[i].Content = HttpUtility.HtmlDecode(stats.answers[i]);
            }

            _time = TimeSpan.FromSeconds(TimeForEachQuestion - 1);

            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                TimeLeft.Text = _time.ToString("c");
                if (_time == TimeSpan.Zero)
                {
                    TimeLeft.Text = "WAITING";
                    this.song.Stop();
                    this.drums.Play();
                    _timer.Stop();
                }
                _time = _time.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);

            _timer.Start();

            SetButtonsToNormal();

            this.NowQuestion += 1;
            UpdateNumQuestions();
        }

        private void onLeaveGame(Response response)
        {
            var stats = response.ToData<Responses.LeaveGame>();
            if (stats == null) return;
            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            chat.Close();
            scoreBoard.Close();
            this.Close(); //only if you want to close the current form.
            song.Stop();
            this.drums.Stop();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            if (Room.Room.AreYouSure("Exit the game"))
            {
                var submitAnswer = new Requests.LeaveGameReq();
                Communicator communicator = Communicator.Instance;
                communicator.TryToSend(submitAnswer);
            }
        }
    }
}