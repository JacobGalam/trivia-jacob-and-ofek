﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Responses = TriviaFront.Classes.Networking.Responses;

using ControlzEx.Theming;
using MahApps.Metro.Controls;

namespace TriviaFront.Game
{
    /// <summary>
    /// Interaction logic for ScoreBoard.xaml
    /// </summary>
    public partial class ScoreBoard : MetroWindow
    {
        public ScoreBoard(List<string> users)
        {
            InitializeComponent();
            ThemeManager.Current.ChangeTheme(this, "Dark.Orange");

            foreach (var username in users)
            {
                addUser(username, 0);
            }
        }

        public void updateScoreBoard(List<Responses.Score> scores)
        {
            Scoreslist.Items.Clear();
            foreach (var item in scores.OrderBy(x => -x.score))
            {
                addUser(item.username, item.score);
            }
        }

        private void addUser(string username, float score)
        {
            Scoreslist.Items.Add($"{username}: {score}");
        }
    }
}