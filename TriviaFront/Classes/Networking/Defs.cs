﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaFront.Classes.Networking
{
    public static class Defs
    {
        public const int Port = 8826;
        public const string Ip = "127.0.0.1"; // 89.138.136.67

        public enum Codes : int
        {
            Error,
            Login,
            Signup,
            Logout,
            GetRooms,
            GetPlayersInRoom,
            JoinRoom,
            ExitRoom,
            CreateRoom,
            UserStats,
            Tops,
            CloseRoom,
            StartGame,
            GetRoomState,
            UserLeave,
            UserJoin,
            LeaveGame,
            GetQuestion,
            SubmitAnswer,
            PlayerResults,
            GetGameResults,
            updateRightAnswer,
            StartGamedata,
            SendMessageChat,
            KickUser,
            BanUser,
            AddQuestion,
            RemoveQuestion,
            GetAllCategories,
            GetUserQuestions
        }
    }
}