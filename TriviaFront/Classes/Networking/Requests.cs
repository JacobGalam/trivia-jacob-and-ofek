﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TriviaFront.Classes.Networking
{
    namespace Requests
    {
        public interface IRequest
        {
            [JsonIgnore]
            byte Code
            {
                get;
            }
        }

        public class Login : IRequest
        {
            public string username;
            public string password;

            byte IRequest.Code => 0;
        }

        public class Signup : IRequest
        {
            public string username;
            public string password;
            public string email;

            byte IRequest.Code => 1;
        }

        public class Logout : IRequest
        {
            byte IRequest.Code => 0;
        }

        public class GetRooms : IRequest
        {
            byte IRequest.Code => 1;
        }

        public class GetPlayersInRoom : IRequest
        {
            public string roomName;

            byte IRequest.Code => 2;
        }

        public class GetTop : IRequest
        {
            public int maxUsers = 0;

            byte IRequest.Code => 3;
        }

        public class GetUserStats : IRequest
        {
            public string userName = "";

            byte IRequest.Code => 4;
        }

        public class JoinRoom : IRequest
        {
            public string roomName;
            public string password;

            byte IRequest.Code => 5;
        }

        public class CreateRoom : IRequest
        {
            public string roomName = "";
            public int maxUsers = 0;
            public int questionCount = 0;
            public int answerTimeout = 0;
            public string password = "";
            public string category = "";

            byte IRequest.Code => 6;
        }

        public class AddQuestion : IRequest
        {
            public string question;
            public string rightAnswer;
            public List<string> wrongAnswers = new List<string>();
            public string category;
            public string diff;

            byte IRequest.Code => 7;
        }

        public class removeQuestion : IRequest
        {
            public string questionToRemove;
            byte IRequest.Code => 8;
        }

        public class getCategories : IRequest
        {
            byte IRequest.Code => 9;
        }

        public class getUserQuestions : IRequest
        {
            byte IRequest.Code => 10;
        }

        public class ExitRoom : IRequest
        {
            public string roomName;

            byte IRequest.Code => 0;
        }

        public class StartGame : IRequest
        {
            public int status;

            byte IRequest.Code => 1;
        }

        public class SubmitAnswer : IRequest
        {
            public string answer;

            byte IRequest.Code => 0;
        }

        public class LeaveGameReq : IRequest
        {
            public int status;

            byte IRequest.Code => 1;
        }

        public class KickUserReq : IRequest
        {
            public string kickedUser;

            byte IRequest.Code => 2;
        };

        public class BanUserReq : IRequest
        {
            public string bannedUser;

            byte IRequest.Code => 3;
        };

        public class SendMessageReq : IRequest
        {
            public string msg;

            byte IRequest.Code => 5;
        };
    }
}