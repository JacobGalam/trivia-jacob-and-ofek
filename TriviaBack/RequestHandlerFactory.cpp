#include "RequestHandlerFactory.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* m_database)
	:
	m_database(m_database), m_loginManager(LoginManager(m_database)), m_statisticsManager(StatisticsManager(m_database)), m_triviaManager(TriviaManager(m_database)), m_gameManager(GameManager(m_database))
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler* loginRH = new LoginRequestHandler(m_loginManager, this);

	return loginRH;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(string userName)
{
	MenuRequestHandler* menuRH = new MenuRequestHandler(this->m_roomManager, this->m_triviaManager, this->m_statisticsManager, *this, userName);

	return menuRH;
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(Room& room, LoggedUser lUser)
{
	RoomAdminRequestHandler* roomAdminRH = new RoomAdminRequestHandler(room.m_metadata.roomName, lUser, this->m_roomManager, *this);

	return roomAdminRH;
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room& room, LoggedUser lUser)
{
	return new RoomMemberRequestHandler(room.m_metadata.roomName, lUser, this->m_roomManager, *this);;
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(unsigned int id, LoggedUser user)
{
	return new GameRequestHandler(this->getGameManager().getGame(id), user, this->m_gameManager, *this);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return this->m_statisticsManager;
}

GameManager& RequestHandlerFactory::getGameManager()
{
	return this->m_gameManager;
}