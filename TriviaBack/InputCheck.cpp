#include "InputCheck.h"

string InputCheck::defaultCheck(string input)
{
	if (input.find("'") != std::string::npos)
	{
		return string("Can't have \"'\" inside");
	}

	return string();
}

void InputCheck::callDefaultCheck(string input, string InputName)
{
	string msg = InputCheck::defaultCheck(input);
	if (!msg.empty())
	{
		auto temp = InputName + " " + msg;
		throw std::exception(temp.c_str());
	}
}

void InputCheck::Max(int input, int max, string InputName)
{
	if (input > max)
	{
		auto temp = InputName + " can't be bigger than " + std::to_string(max);
		throw std::exception(temp.c_str());
	}
}

void InputCheck::Min(int input, int min, string InputName)
{
	if (input < min)
	{
		auto temp = InputName + " can't be smaller than " + std::to_string(min);
		throw std::exception(temp.c_str());
	}
}

void InputCheck::MinMax(string input, int min, int max, string InputName)
{
	if (min > -1)
		InputCheck::Min(input, min, InputName);

	InputCheck::Max(input, max, InputName);

	InputCheck::callDefaultCheck(input, InputName);
}

void InputCheck::Max(string input, int max, string InputName)
{
	if (input.size() > max)
	{
		auto temp = InputName + " size can't be bigger than " + std::to_string(max);
		throw std::exception(temp.c_str());
	}
}

void InputCheck::Min(string input, int min, string InputName)
{
	if (input.size() < min)
	{
		auto temp = InputName + " size can't be smaller than " + std::to_string(min);
		throw std::exception(temp.c_str());
	}
}

void InputCheck::NotZeroAndMax(string input, int max, string InputName)
{
	if (input.empty())
	{
		auto temp = InputName + " can't be empty";
		throw std::exception(temp.c_str());
	}

	InputCheck::Max(input, max, InputName);

	InputCheck::callDefaultCheck(input, InputName);
}

void InputCheck::MinMax(int input, int min, int max, string InputName)
{
	InputCheck::Min(input, min, InputName);
	InputCheck::Max(input, max, InputName);
}

void InputCheck::NotZeroAndMax(int input, int max, string InputName)
{
	if (input == 0)
	{
		auto temp = InputName + " can't be zero";
		throw std::exception(temp.c_str());
	}
	InputCheck::Max(input, max, InputName);
}