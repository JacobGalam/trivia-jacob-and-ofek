#pragma once
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "TriviaManager.h"
#include "GameManager.h"
#include "IDatabase.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	/*
	c'tor that gets the db as param and sets its field
	input: IDatabase* m_database
	output: none
	*/
	RequestHandlerFactory(IDatabase* m_database);

	/*
	creates LoginRequestHandler by using its c'tor that uses this object and Login
	input: none
	output: LoginRequestHandler: new handler
	*/
	LoginRequestHandler* createLoginRequestHandler();
	/*
	creates MenuRequestHandler by using its c'tor that uses the username param, roomManager, this, statisticsManager
	input: string userName - user requested the handler
	output: MenuRequestHandler: new handler
	*/
	MenuRequestHandler* createMenuRequestHandler(string userName);
	/*
	creates RoomAdminRequestHandler by using its c'tor that uses param room, param lUser and this
	input: Room& room - the room that needs to be created, LoggedUser lUser - the user that opens the room
	output: RoomAdminRequestHandler: new handler
	*/
	RoomAdminRequestHandler* createRoomAdminRequestHandler(Room& room, LoggedUser lUser);
	/*
	creates RoomMemberRequestHandler by using its c'tor that uses param room, param lUser, roomManager and this
	input: Room& room, LoggedUser lUser
	output: RoomMemberRequestHandler: new handler
	*/
	RoomMemberRequestHandler* createRoomMemberRequestHandler(Room& room, LoggedUser lUser);
	/*
	creates GameRequestHandler by using its c'tor that uses param id, param user and this
	input: unsigned int id - the id of the game, LoggedUser user - the user that uses the game
	output: GameRequestHandler: new handler
	*/
	GameRequestHandler* createGameRequestHandler(unsigned int id, LoggedUser user);

	//getters
	LoginManager& getLoginManager();
	RoomManager& getRoomManager();
	StatisticsManager& getStatisticsManager();
	GameManager& getGameManager();

private:
	IDatabase* m_database;
	LoginManager m_loginManager;
	StatisticsManager m_statisticsManager;
	TriviaManager m_triviaManager;
	RoomManager m_roomManager;
	GameManager m_gameManager;
};

#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "GameRequestHandler.h"
