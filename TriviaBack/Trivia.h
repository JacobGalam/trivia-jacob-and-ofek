#pragma once
#include <string>
#include "SqlDataBaseCore.h"

using std::string;

class RoundData
{
public:
	RoundData() = default;

public:
	int id;
	string category;
	string difficulty;
	string correctAnswer;
	string question;
	std::vector<string> incorrectAnswers;
};
