#pragma once
#include "DefaultMemberRequstHandler.h"
#include "Communicator.h"

namespace RoomMemberOptions
{
	enum Codes
	{
		leaveRoom,
	};
}

class RoomMemberRequestHandler : public DefaultMemberRequstHandler
{
public:
	/*
	c'tor of the RoomMemberRequestHandler. sets the fields roomName, m_user, m_roomManager and m_handlerFactory by params
	input: string roomName, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory
	output: none
	*/
	RoomMemberRequestHandler(string roomName, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory);

	/*
	a function map that indicates the index of every method in the class by the needed request.
	input: none
	output: handleRequestFunc_map&: the map of the methods in the class
	*/
	virtual handleRequestFunc_map& optionsMap() override;

	virtual void onExit() override;
private:
	/*
	deserializes leaveRoom req. User leaves the room, removes itself from it and sends the other users UserLeave response.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static leaveRoom(IRequestHandler* self, RequestInfo rInfo);
};
