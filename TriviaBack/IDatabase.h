#pragma once
#include <string>
#include <vector>

#include "Requests.h"
#include "Responses.h"
#include "Trivia.h"

struct Answer
{
	string answer;
	time_t responseTime;
	int trivaID;
};

class IDatabase
{
public:
	/*
	Open the DB
	Input: None
	Output: If can
	*/
	virtual bool open() = 0;
	// clear all userData
	virtual bool clearAllUsersData() = 0;
	/*
	Close the DB
	Input: None
	Output: None
	*/
	virtual void close() = 0;
	/*
	Check if the user exist
	Input: User name to search
	Output: If exist
	*/
	virtual bool doesUserExist(string userName) = 0;
	virtual bool doesTriviaExist(string question, string username) = 0;
	virtual bool doesCategoryExist(string category) = 0;
	/*
	Check if the password match
	Input: Password
	Output: If match
	*/
	virtual bool doesPasswordMatch(string username, string password) = 0;
	/*
	add new user to dataBase
	Input: Username, password and email of the new user
	Output: None
	*/
	virtual void addNewUser(string username, string password, string email) = 0;
	virtual void addNewTrivia(string question, string rightAnswer, std::vector<string> wrongAnswers, string category, string diff, string username) = 0;
	virtual bool removeTrivia(string question, string username) = 0;
	/*
	Get a rand Trivias objects form the dataBase
	Warning: Can return the same trivias one after another
	Input: how much Trivias
	Output: vector of question and answerers
	*/
	virtual std::vector<RoundData> getRandTrivias(int numberOfTrivias, string category) = 0;

	// return the id of the game. if return -1 can't start the game.
	virtual int StartNewGame() = 0;
	// when the game end, after calling 'StartNewGame'
	virtual bool finishGame(string username, int gameId, float score, std::vector<Answer> answers) = 0;

	// return seconds
	virtual double getPlayerAverageAnswerTime(string userName) = 0;

	virtual unsigned long long getNumOfCorrectAnswers(string userName) = 0;
	virtual unsigned long long getNumOfTotalAnswers(string userName) = 0;
	virtual unsigned long long getNumOfPlayerGames(string userName) = 0;
	virtual unsigned long long getPlayerTotalScore(string userName) = 0;
	virtual std::vector<Responses::Score> getTops(int numOfTops) = 0;
	virtual std::vector<string> getCategories() = 0;
	virtual std::vector<string> getUserQuestions(string username) = 0;
	virtual int getNumOfQuestions(string username) = 0;
	virtual int getNumOfCategoryQuestions(string category) = 0;
};
