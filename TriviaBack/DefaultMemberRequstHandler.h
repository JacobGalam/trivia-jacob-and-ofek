#pragma once

#include "RequestHandlerFactory.h"
#include "SendMsgRequestHandler.h"

/*
Options of the handle
*/
namespace DefaultRoomOptions
{
	enum Codes
	{
		getUserStatistics = 4
	};
}

/*
4 - getUserStatistics
5 - sendMessage
*/
class DefaultMemberRequstHandler : public SendMsgRequestHandler
{
public:
	/*
	Ctor of the DefaultMemberRequstHandler. handle getRoomState, getUserStatistics.
	input: room name, user, room Manager, RequestHandlerFactory
	output: None
	*/
	DefaultMemberRequstHandler(string roomName, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory);

	// Inherited via IRequestHandler
	virtual handleRequestFunc_map& optionsMap() override;

	/*
	What to do when user on exit
	input: None
	output: None
	*/
	virtual void onExit() override;
protected:
	string roomName;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandlerFactory& m_handlerFactory;

	/*
	Send massage to all users expect the 'except' user
	input: the msg and the except user
	output: None
	*/
	template <class MsgType>
	void sendMsgToUsers(MsgType msg, LoggedUser except)
	{
		for (auto const& LoggedUser : this->m_roomManager.getRoomState(this->roomName).getAllUsers())
		{
			if (LoggedUser.getUsername() == except.getUsername())
			{
				continue;
			}
			Communicator::getInstance().sendResponse(LoggedUser, msg);
		}
	}

	/*
	Send massage to all users
	input: The msg and the except user
	output: None
	*/
	static void sendGameMessages(IRequestHandler* self, Responses::SendMessageChat msg, LoggedUser sender);
	/*
	Get the state of the users
	input: The object and info about request
	output: New handler and what to send
	*/
	RequestResult static getRoomState(IRequestHandler* self, RequestInfo rInfo);
	/*
	Get stats about the user
	input: The object and info about request
	output: New handler and what to send
	*/
	RequestResult static getUserStatistics(IRequestHandler* self, RequestInfo rInfo);
};
