#pragma once
#include <string>
#include <vector>
#include <mutex>

#include "IDatabase.h"

using std::string;
using std::vector;

class TriviaManager
{
public:

	// menage the user trivia inset, remove and so on
	/*
	init the Class
	input: dataBase pointer
	output: None
	*/
	TriviaManager(IDatabase* iDataBase);
	/*
	Get the questions of user
	input: which user
	output: his Questions
	*/
	vector<string> getUserQuestions(string username);
	/*
	Get All Categories of the server
	input: None
	output: All the Categories
	*/
	vector<string> getCategories();
	/*
	remove trivia
	input: Which user and which question
	output: None
	*/
	void removeTrivia(string question, string username);
	/*
	add trivia
	input: data of the trivia
	output: None
	*/
	void addTrivia(string question, string rightAnswer, std::vector<string> wrongAnswers, string category, string diff, string username);
	/*
	get How much Questions this category have
	input: Which category
	output: How much Questions
	*/
	int getNumOfCategoryQuestions(string category);
private:
	IDatabase* m_database;
	std::mutex m_managerMutex;
};