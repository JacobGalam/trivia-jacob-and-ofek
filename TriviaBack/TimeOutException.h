#pragma once
#include <exception>

class TimeOutException : std::exception
{
public:
	bool isSend;
	/*
	If user timeout, if user not send or receive
	input: If need to close the connection at the client
	output: None
	*/
	TimeOutException(bool isSend)
	{
		this->isSend = isSend;
	}
};
