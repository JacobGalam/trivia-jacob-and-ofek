#pragma once
#include "Buffer.h"
#include "Responses.h"

#define ADD_SERIALIZE(CLASS, CODE) static Buffer serializeResponse(CLASS respond) { return JsonResponsePacketSerializer::toBuff(respond, CODE); }
#define MAKE_RES_FN_NAME(CLASS) ADD_SERIALIZE(Responses::CLASS, ResponesCodes::Codes::CLASS)

namespace ResponesCodes
{
	enum Codes
	{
		Error,
		Login,
		Signup,
		Logout,
		GetRooms,
		GetPlayersInRoom,
		JoinRoom,
		ExitRoom,
		CreateRoom,
		UserStats,
		Tops,
		CloseRoom,
		StartGame,
		GetRoomState,
		UserLeave,
		UserJoin,
		LeaveGame,
		GetQuestion,
		SubmitAnswer,
		PlayerResults,
		GetGameResults,
		updateRightAnswer,
		StartGamedata,
		SendMessageChat,
		KickUser,
		BanUser,
		AddQuestion,
		RemoveQuestion,
		GetAllCategories,
		GetUserQuestions,
	};
};

using namespace Responses;
class JsonResponsePacketSerializer
{
public:
	/*
	Respond to buffer for sending
	Input: The respond
	Output: Data buffer for sending
	*/
	// TODO make the codes enum
	MAKE_RES_FN_NAME(Error);
	MAKE_RES_FN_NAME(Login);
	MAKE_RES_FN_NAME(Signup);
	MAKE_RES_FN_NAME(Logout);
	MAKE_RES_FN_NAME(GetRooms);
	MAKE_RES_FN_NAME(GetPlayersInRoom);
	MAKE_RES_FN_NAME(JoinRoom);
	MAKE_RES_FN_NAME(ExitRoom);
	MAKE_RES_FN_NAME(CreateRoom);
	MAKE_RES_FN_NAME(UserStats);
	MAKE_RES_FN_NAME(Tops);
	MAKE_RES_FN_NAME(CloseRoom);
	MAKE_RES_FN_NAME(StartGame);
	MAKE_RES_FN_NAME(GetRoomState);
	MAKE_RES_FN_NAME(UserLeave);
	MAKE_RES_FN_NAME(UserJoin);
	MAKE_RES_FN_NAME(LeaveGame);
	MAKE_RES_FN_NAME(GetQuestion);
	MAKE_RES_FN_NAME(SubmitAnswer);
	MAKE_RES_FN_NAME(PlayerResults);  // not return just one
	MAKE_RES_FN_NAME(GetGameResults);
	MAKE_RES_FN_NAME(updateRightAnswer);
	MAKE_RES_FN_NAME(StartGamedata);
	MAKE_RES_FN_NAME(SendMessageChat);
	MAKE_RES_FN_NAME(KickUser);
	MAKE_RES_FN_NAME(BanUser);
	MAKE_RES_FN_NAME(AddQuestion);
	MAKE_RES_FN_NAME(RemoveQuestion);
	MAKE_RES_FN_NAME(GetAllCategories);
	MAKE_RES_FN_NAME(GetUserQuestions);

	/*
	Respond to buffer for sending
	Input: The respond
	Output: Data buffer for sending
	*/
	/*
	Respond to buffer for sending
	Input: The respond
	Output: Data buffer for sending
	*/
private:
	template<class RequestStruct>
	static Buffer toBuff(RequestStruct response, unsigned char code)
	{
		Buffer returnBuff;
		returnBuff.insertCode(code);

		json asJson = toJson(response);
		string jString = asJson.dump();
		returnBuff.insertNumber(jString.size());
		returnBuff.insertStr(jString);

		return returnBuff;
	}
};
