#pragma once
#include <string>

using std::string;

class InputCheck
{
public:
	/*
	Check if the string len is more or less than the max or min
	input: input, min size, max size and input name for show in error
	output: None (throw an error)
	*/
	static void MinMax(string input, int min, int max, string InputName);
	/*
	Check if string len not zero or more than the max
	input: input, max size and input name for show in error
	output: None (throw an error)
	*/
	static void NotZeroAndMax(string input, int max, string InputName);
	/*
	Check if string len not zero or more than the max
	input: input, max size and input name for show in error
	output: None (throw an error)
	*/
	static void MinMax(int input, int min, int max, string InputName);
	/*
	Check if len not zero and not max
	input: input, max size and input name for show in error
	output: None (throw an error)
	*/
	static void NotZeroAndMax(int input, int max, string InputName);
private:
	/*
	Check if max
	input: input, max and input name for show in error
	output: None
	*/
	static void Max(string input, int max, string InputName);
	/*
	Check if min
	input: input, min and input name for show in error
	output: None
	*/
	static void Min(string input, int min, string InputName);
	/*
	This check all user input
	input: input
	output: The msg for user
	*/
	static string defaultCheck(string input);
	/*
	Call the 'defaultCheck' and throw an error if return string not empty
	input: input
	output: None (throw an error)
	*/
	static void callDefaultCheck(string input, string InputName);

	/*
	Check if Max
	input: input, Max and input name for show in error
	output: None
	*/
	static void Max(int input, int max, string InputName);
	/*
	Check if min
	input: input, min and input name for show in error
	output: None
	*/
	static void Min(int input, int min, string InputName);
};
