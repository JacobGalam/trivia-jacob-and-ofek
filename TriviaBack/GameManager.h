#pragma once
#include <mutex>

#include "IDatabase.h"
#include "Game.h"

class GameManager
{
public:
	GameManager(IDatabase* m_database);
	unsigned int createGame(Room& room);
	void deleteGame(unsigned int id);
	Game& getGame(unsigned int id);

private:
	std::mutex m_managerMutex;
	IDatabase* m_database;
	std::map<unsigned int, Game*> m_games;
};
