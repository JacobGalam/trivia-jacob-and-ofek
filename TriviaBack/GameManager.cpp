#include "GameManager.h"

GameManager::GameManager(IDatabase* m_database) : m_database(m_database)
{
}

unsigned int GameManager::createGame(Room& room)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	unsigned int gameId = this->m_database->StartNewGame();

	auto trivias = this->m_database->getRandTrivias(room.m_metadata.questionCount, room.m_metadata.category);

	this->m_games[gameId] = new Game(room, trivias, gameId);

	return gameId;
}

void GameManager::deleteGame(unsigned int id)
{
	auto game = this->m_games.find(id);
	if (game == this->m_games.end()) return;
	delete game->second;
	this->m_games.erase(game);
}

Game& GameManager::getGame(unsigned int id)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	return *this->m_games[id];
}