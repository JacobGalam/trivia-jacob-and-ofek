#pragma once
#include <list>
#include <io.h>
#include <iostream>
#include <vector>
#include <set>
#include <TriviaBack\sha256.h>
#include <sstream>
#include <iterator>

#include "IDatabase.h"
#include "SqlDataBaseCore.h"

#include "Responses.h"

class SqlDataBase : public IDatabase
{
public:
	/*
	Init the class
	Input: None
	Output: None
	*/
	SqlDataBase();

	// Inherited via IDatabase
	virtual bool open() override;
	virtual bool clearAllUsersData() override;

	virtual bool doesUserExist(string userName) override;
	virtual bool doesTriviaExist(string question, string username) override;
	virtual bool doesCategoryExist(string category) override;

	// Inherited via IDatabase
	virtual std::vector<RoundData> getRandTrivias(int numberOfTrivias, string category) override;

	virtual double getPlayerAverageAnswerTime(string userName) override;
	virtual unsigned long long getNumOfCorrectAnswers(string userName) override;
	virtual unsigned long long getNumOfTotalAnswers(string userName) override;
	virtual unsigned long long getNumOfPlayerGames(string userName) override;

	// Inherited via IDatabase
	virtual bool doesPasswordMatch(string username, string password) override;
	virtual void addNewUser(string username, string password, string email) override;
	virtual void addNewTrivia(string question, string rightAnswer, std::vector<string> wrongAnswers, string category, string diff, string username) override;
	virtual bool removeTrivia(string question, string username) override;

	virtual int StartNewGame() override;

	virtual bool finishGame(string username, int gameId, float score, std::vector<Answer> answers) override;

	// Inherited via IDatabase
	virtual unsigned long long getPlayerTotalScore(string userName) override;
	virtual std::vector<Responses::Score> getTops(int numOfTops) override;
	virtual std::vector<string> getCategories() override;
	virtual std::vector<string> getUserQuestions(string username) override;
	virtual int getNumOfQuestions(string username) override;
	virtual int getNumOfCategoryQuestions(string category) override;

private:
	int getTriviaId(string question, string username);
	int getUserId(string username);
	int getAnswerId(int trivaID, string answer);
	Records getCommits(string userName);
	std::vector<Record> getScores(string userName);
	RoundData getTrivia(Record triviaRecord);
	std::string randomSalt(size_t length);
private:
	SqlDataBaseCore core;

	// Inherited via IDatabase
	virtual void close() override;
};
