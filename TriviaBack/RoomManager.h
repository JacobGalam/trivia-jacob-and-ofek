#pragma once
#include <map>
#include <mutex>

#include "Room.h"
#include "Requests.h"

using std::map;

class RoomManager
{
public:
	/*
	creating a new room, putting its data struct in it and its LoggedUser + adding it to the m_rooms map
	input: LoggedUser lUser - the user created the room, RoomData rData - the data the the new room includes
	output: none
	*/
	Room& createRoom(LoggedUser lUser, Requests::CreateRoomReq rData);
	void deleteRoom(string name);
	/*
	removing a room from the rooms map by its id (by searching for the key id)
	input: int id - the id of the room that needs to be removed
	output: none
	*/
	/*
	getter for the state of the room by searching for its id and returning its "isActive" field in the metadata (indicates the state of the room)
	input: int id - the room that needs to be searched for getting its state
	output: none
	*/
	Room& getRoomState(string name);
	/*
	getter for the rooms map field
	input: none
	output: the map of the m_rooms field
	*/
	Responses::GetRooms getRooms();
private:
	std::mutex m_managerMutex;
	map<string, Room> m_rooms;  // name to room
};
