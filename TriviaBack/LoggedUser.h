#pragma once
#include <string>

using std::string;

class LoggedUser
{
public:
	LoggedUser() = default;
	/*
	Init the class
	Input: The user name
	Output: userName
	*/
	LoggedUser(string name);
	bool operator<(const LoggedUser& lhs) const;
	bool operator==(const LoggedUser& name);
	/*
	Get the userName
	Input: None
	Output: userName
	*/
	string getUsername() const;

private:
	string m_username;
};
