#include "LoggedUser.h"

LoggedUser::LoggedUser(string name)
{
	this->m_username = name;
}

bool LoggedUser::operator<(const LoggedUser& lhs) const
{
	return this->m_username < lhs.m_username;
}

bool LoggedUser::operator==(const LoggedUser& name)
{
	return this->m_username == name.m_username;
}

string LoggedUser::getUsername() const
{
	return this->m_username;
}