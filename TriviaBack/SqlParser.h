#pragma once

#include <string>
using std::string;

class SqlParser
{
public:
	template<class Type>
	static string Commmnad(Type value)
	{
		if (std::is_integral<Type>::value)
		{
			return std::to_string(value);
		}
		throw std::exception("Can not make the type to string");
	}

	// use as static class or namespace
	template< typename FIRST, typename... REST >
	static string Commmnad(const FIRST first, const REST... rest)
	{
		return SqlParser::Commmnad(first) + " " + SqlParser::Commmnad(rest...);
	}

	static string AddToList();

	template< typename type >
	static string AddToList(type value)
	{
		return SqlParser::Commmnad(value);
	}

	template< typename FIRST, typename... REST >
	static string AddToList(const FIRST first, const REST... rest)
	{
		return SqlParser::AddToList(first) + ", " + SqlParser::AddToList(rest...);
	}

	template< typename FIRST, typename... REST >
	static string StartList(const FIRST first, const REST... rest)
	{
		return "(" + SqlParser::AddToList(first, rest...) + ")";
	}
	/*
	end of commmnad
	*/
	static string Commmnad();

	/*
	a text, add '
	*/
	static string Commmnad(const string data);
	static string Commmnad(const float data);

	/*
	a int
	*/
	static string Commmnad(const int data);

	/*
	command. A regaler command text without '
	*/
	static string Commmnad(const char* data);
};
