#pragma once
#include <vector>
#include <map>
#include <regex>
#include <mutex>

#include "IDatabase.h"
#include "LoggedUser.h"
#include "Socket.h"

using std::vector;
using std::map;

class LoginManager
{
	// Managing db of the login
public:
	/*
	Init empty
	Input: None
	Output: None
	*/
	LoginManager() = default;
	/*
	Init with a db
	Input: The db
	Output: None
	*/
	LoginManager(IDatabase* db);
	/*
	Add new login
	Input: username and password
	Output: If succeeded
	*/
	void newLog(string username, string password, Socket* sock);
	/*
	Add new sign up
	Input: username, password and email
	Output: If succeeded
	*/
	bool newSignup(string username, string password, string email, Socket* sock);
	/*
	Logout a user
	Input: the user username
	Output: If succeeded
	*/
	bool logoutUser(string username);
	/*
	Using regex in order to check if the email is valid or not (has @ and .)
	input: const std::string& email - the string of the email
	output: bool: true - the email is valid, false - the email is invalid
	*/
	bool isEmailValid(const std::string& email); //*BONUS*
	/*
	checking if the password is strong enough
	input: const std::string& password - the string of the password
	output: bool: true - the password is valid, false - the password is invalid
	*/
	bool isPasswordStrong(const std::string& password); //*BONUS*

	static map<LoggedUser, Socket*> usersSock;

private:
	std::mutex m_managerMutex;
	/*
	Creating a new LoggedUser by the username, adding it to the loggedusers map and to the sockets map handling the users.
	input: string username - the name of the new user, Socket* sock - the sock that handles the user's reqs
	output: none
	*/
	void addUser(string username, Socket* sock);
	IDatabase* m_database;
	map<string, LoggedUser> m_loggedUsers;
};
