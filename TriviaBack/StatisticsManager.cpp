#include "StatisticsManager.h"

StatisticsManager::StatisticsManager(IDatabase* iDataBase)
{
	this->m_database = iDataBase;
}

Responses::Tops StatisticsManager::getHighScore(int numOfTops)
{
	Responses::Tops tops;

	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	for (auto top : this->m_database->getTops(numOfTops))
	{
		tops.tops.push_back(top);
	}

	return tops;
}

UserStats StatisticsManager::getUserStatistics(string username)
{
	UserStats returnStats;

	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	returnStats.numRightAns = this->m_database->getNumOfCorrectAnswers(username);
	returnStats.numOfGame = this->m_database->getNumOfPlayerGames(username);
	returnStats.numWrongAns = this->m_database->getNumOfTotalAnswers(username) - returnStats.numRightAns;
	returnStats.avrageTimeToAns = this->m_database->getPlayerAverageAnswerTime(username);
	returnStats.numOfQuestions = this->m_database->getNumOfQuestions(username);

	return returnStats;
}

void StatisticsManager::postStatistics(string username, int gameId, float score, std::vector<Answer> answers)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	this->m_database->finishGame(username, gameId, score, answers);
}