#pragma once
#include <vector>
#include <iterator>     // std::iterator_traits
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "InputCheck.h"

using std::vector;

/*
Options of the handle
*/
namespace OptionCode
{
	enum SendMsgOption
	{
		sendMessage = 5
	};
}

typedef void (SendDataMsgFunc)(IRequestHandler*, Responses::SendMessageChat, LoggedUser);

/*
5 - sendMessage
*/
class SendMsgRequestHandler : public IRequestHandler
{
public:

	/*
	If user timeout, if user not send or receive
	input: If need to close the connection at the client
	output: None
	*/
	SendMsgRequestHandler(SendDataMsgFunc* sendDataFunc, LoggedUser& user);
	// Inherited via IRequestHandler
	virtual handleRequestFunc_map& optionsMap() override;
private:
	/*
	option of the send msg
	input: this object and the request info
	output: The new Handle and what to send
	*/
	RequestResult static sendMessage(IRequestHandler* self, RequestInfo rInfo); //bonus
	SendDataMsgFunc* sendDataFunc;
	LoggedUser& user;

	// Inherited via IRequestHandler
	virtual void onExit() override;
};
