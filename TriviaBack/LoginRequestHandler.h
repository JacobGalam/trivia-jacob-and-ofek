#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "RequestResult.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "Responses.h"
#include "InputCheck.h"

namespace loginOptions
{
	enum Codes
	{
		login,
		signup
	};
}

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:

	/*
	a function map that indicates the index of every method in the class by the needed request.
	input: none
	output: handleRequestFunc_map&: the map of the methods in the class
	*/
	virtual handleRequestFunc_map& optionsMap() override;

	/*
	The c'tor that sets the private fields m_loginManager and m_handlerFactory
	*/
	LoginRequestHandler(LoginManager& lManager, RequestHandlerFactory* requestHandlerF);

	virtual void onExit() override {};

private:
	/*
	deserializing the login req and validating the info. if it is valid, the user logins to the server.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static login(IRequestHandler* self, RequestInfo what);
	/*
	deserializing the signup req and validating the info. if it is valid, the user signsup to the server.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static signup(IRequestHandler* self, RequestInfo what);

	LoginManager& m_loginManager;
	RequestHandlerFactory* m_handlerFactory;
};
