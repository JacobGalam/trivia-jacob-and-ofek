#pragma once
#include <algorithm>

#include "nlohmann\json.hpp"
using json = nlohmann::json;

#define PROPERTY(CLASS, MEMBER) property(&CLASS::MEMBER, #MEMBER)

// types
#define STD_REGULAR 0
#define USER_CLASS 1
#define VEC_USER_CLASS 2
#define VEC_IN_VEC_USER_CLASS 3

template<typename Class, typename T>
struct PropertyImpl {
	constexpr PropertyImpl(T Class::* aMember, const char* aName) : member{ aMember }, name{ aName } {}

	using Type = T;

	T Class::* member;
	const char* name;
};

template<typename Class, typename T>
constexpr auto property(T Class::* member, const char* name) {
	return PropertyImpl<Class, T>{member, name};
}

template <typename T, T... S, typename F>
constexpr void for_sequence(std::integer_sequence<T, S...>, F&& f) {
	(static_cast<void>(f(std::integral_constant<T, S>{})), ...);
}

template <typename T>
struct is_iterable {
	static const bool value = false;
};

template <typename T, typename Alloc>
struct is_iterable<std::vector<T, Alloc> > {
	static const bool value = true;
};

template <typename Type>
constexpr int callFuncByType()
{
	if constexpr (std::is_same<std::string, Type>::value)  // string exception
	{
		return STD_REGULAR;
	}
	else if constexpr (is_iterable<Type>::value)
	{
		//constexpr int vectorElementType = callFuncByType<ElementType>();
		constexpr int vectorElementType = callFuncByType<Type::value_type>();

		if constexpr (vectorElementType == STD_REGULAR)
		{
			return STD_REGULAR;
		}
		else if constexpr (vectorElementType == USER_CLASS)
		{
			return VEC_USER_CLASS;
		}
		else if constexpr (vectorElementType == VEC_USER_CLASS)
		{
			return VEC_IN_VEC_USER_CLASS;
		}
	}
	else if constexpr (std::is_class<Type>::value)
	{
		return USER_CLASS;
	}
	else
	{
		return STD_REGULAR;
	}
}

template<typename T>
T fromJson(const json& data);

template<typename T>
T test(const json& data)
{
	T temp;

	for (auto js : data)
	{
		temp.push_back(fromJson<T::value_type>(js));
	}

	return temp;
}

template<typename T>
T fromJson(const json& data) {
	T object;

	// We first get the number of properties
	constexpr auto nbProperties = std::tuple_size<decltype(T::properties)>::value;

	// We iterate on the index sequence of size `nbProperties`
	for_sequence(std::make_index_sequence<nbProperties>{}, [&](auto i) {
		// get the property
		constexpr auto property = std::get<i>(T::properties);

		// get the type of the property
		using Type = typename decltype(property)::Type;

		constexpr auto name = property.name;
		constexpr auto member = property.member;

		auto field = object.*(property.member);

		constexpr auto fieldType = callFuncByType<Type>();

		if constexpr (fieldType == STD_REGULAR)
		{
			object.*(member) = data[name].get<Type>();
		}
		else if constexpr (fieldType == USER_CLASS)
		{
			object.*(member) = fromJson<Type>(data[name]);
		}
		else if constexpr (fieldType == VEC_USER_CLASS)// VEC_IN_VEC_USER_CLASS VEC_USER_CLASS
		{
			for (auto js : data[name])
			{
				(object.*(member)).push_back(fromJson<Type::value_type>(js));
			}
		}
		else if constexpr (fieldType == VEC_IN_VEC_USER_CLASS)// VEC_IN_VEC_USER_CLASS VEC_USER_CLASS
		{
			for (auto js : data[name])
			{
				auto temp = test<Type::value_type>(js);
				(object.*(member)).push_back(temp);
			}
		}
		});

	return object;
}

template<typename T>
json toJson(const T& object);

template <typename T, typename Alloc>
json toJson(const std::vector<T, Alloc>& vec) // vector to json
{
	json temp = {};
	for (auto elemnt : vec)
	{
		temp += toJson(elemnt);
		//temp += toJson<std::vector<T, Alloc>::value_type>(elemnt);
	}
	return temp;
}

template<typename T>
json toJson(const T& object)
{
	json data;

	// We first get the number of properties
	constexpr auto nbProperties = std::tuple_size<decltype(T::properties)>::value;

	// We iterate on the index sequence of size `nbProperties`
	for_sequence(std::make_index_sequence<nbProperties>{}, [&](auto i) {
		// get the property

		constexpr auto property = std::get<i>(T::properties);
		using Type = typename decltype(property)::Type;
		constexpr auto name = property.name;
		constexpr auto member = property.member;

		auto field = object.*(property.member);

		constexpr auto fieldType = callFuncByType<Type>();

		if constexpr (fieldType == STD_REGULAR)
		{
			data[name] = field;
		}
		else if constexpr (fieldType == USER_CLASS)
		{
			data[name] = toJson(field);
		}
		else if constexpr (fieldType == VEC_USER_CLASS || fieldType == VEC_IN_VEC_USER_CLASS)
		{
			data[name] = {};
			for (auto element : field)
			{
				data[name] += toJson(element);
			}
		}
		// set the value to the member
		});

	return data;
}

// example:
//struct Dog
//{
//	std::string barkType;
//	std::string color;
//	int weight = 0;
//
//	// must have!
//	constexpr static auto properties = std::make_tuple(
//		PROPERTY(Dog, barkType),
//		PROPERTY(Dog, color),
//		PROPERTY(Dog, weight)
//	);
//
//	/*
//	use example:
//	json a = toJson(Dog());
//	Dog b = fromJson<Dog>(a);
//	*/
//};