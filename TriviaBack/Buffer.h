#pragma once
#include <vector>
#include <string>

using std::string;
#include "nlohmann/json.hpp"

// TODO 5 bytes side
// for convenience
using json = nlohmann::json;

class Buffer : private std::vector<unsigned char>
{
public:
	/*
	The size of the sent msg from the client transfered to the buffer chunk
	input: unsigned int enteredNum - size of data
	output: the edited buffer
	*/
	Buffer& insertNumber(unsigned int enteredNum);
	/*
	The msg string from the client transfered to the buffer chunk
	input: string enteredStr - the data of the msg
	output: the edited buffer
	*/
	Buffer& insertStr(string enteredStr);
	/*
	The code at the beggining of the msg sent from the client is transfered to the buffer binary sequence
	input: unsigned char enteredStr - the msg code from client
	output: the edited buffer
	*/
	Buffer& insertCode(unsigned char enteredStr);

	using std::vector<unsigned char>::end;
	using std::vector<unsigned char>::begin;

	/*
	Getter for the json from the msg parsed
	input: none
	output: json type of the msg
	*/
	json getJson();
	/*
	Getter for the size of the buffer
	input: none
	output: the size of the buffer
	*/
	unsigned int getSize();
	/*
	Getter for the msg code of the buffer
	input: none
	output: the code of the buffer
	*/
	unsigned char getCode();
};
