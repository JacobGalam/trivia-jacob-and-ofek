#pragma once
#include "Requests.h"
#include "RequestResult.h"
#include "Buffer.h"

#define MAKE_FN_NAME(x) deserialize ## x ## Request
#define ADD_DESERIALIZE(CLASS) static Requests::CLASS MAKE_FN_NAME(CLASS)(Buffer buf) { return toBuff<Requests::CLASS>(buf); }

class JsonRequestPacketDeserializer
{
public:

	/*
	Buffer to respond
	Input: Data buffer for receiving
	Output: The request
	*/
	ADD_DESERIALIZE(LoginReq);
	ADD_DESERIALIZE(SignupReq);
	ADD_DESERIALIZE(GetPlayersInRoomReq);
	ADD_DESERIALIZE(JoinRoomReq);
	ADD_DESERIALIZE(ExitRoomReq);
	ADD_DESERIALIZE(CreateRoomReq);
	ADD_DESERIALIZE(UserStatsReq);
	ADD_DESERIALIZE(TopReq);
	ADD_DESERIALIZE(CloseRoomReq);
	ADD_DESERIALIZE(StartGameReq);
	ADD_DESERIALIZE(GetRoomStateReq);
	ADD_DESERIALIZE(SubmitAnswerReq);
	ADD_DESERIALIZE(GetQuestionReq);
	ADD_DESERIALIZE(LeaveGameReq);
	ADD_DESERIALIZE(GetGameResultReq);
	ADD_DESERIALIZE(SendMessageReq);
	ADD_DESERIALIZE(KickUserReq);
	ADD_DESERIALIZE(BanUserReq);
	ADD_DESERIALIZE(AddQuestionReq);
	ADD_DESERIALIZE(RemoveQuestionReq);

private:
	template<class RequestStruct>
	static RequestStruct toBuff(Buffer buf)
	{
		try
		{
			auto asJson = buf.getJson();
			return fromJson<RequestStruct>(asJson);
		}
		catch (...)
		{
			
			throw std::exception("Cannot handle input");
		}
	}
};
