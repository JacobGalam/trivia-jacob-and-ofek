#pragma once
#include <WinSock2.h>
#include "Buffer.h"
#include "Helper.h"

class Socket
{
public:
	/*
	Make new socket
	input: Win Sock
	output: None
	*/
	Socket(SOCKET sock);
	/*
	Close the Socket
	input: None
	output: None
	*/
	~Socket();
	/*
	Send data to the socket
	input: What to send
	output: None
	*/
	void send(Buffer buff) const;
	/*
	receive data from the socket
	input: None
	output: the response
	*/
	Buffer receive() const;
	SOCKET sock;
};