#include "RoomManager.h"

Room& RoomManager::createRoom(LoggedUser lUser, Requests::CreateRoomReq rData)
{
	// if room exist
	std::lock_guard<std::mutex> lck(this->m_managerMutex);

	if (this->m_rooms.find(rData.roomName) != this->m_rooms.end())
	{
		throw std::exception("There are already room with that name. Please select a different room name");
	}

	auto room = Room(rData);
	room.addUser(lUser);
	this->m_rooms[rData.roomName] = room;

	return this->m_rooms[rData.roomName];
}

void RoomManager::deleteRoom(string name)
{
	this->m_rooms.erase(name);
}

Room& RoomManager::getRoomState(string name)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	return this->m_rooms[name];
}

Responses::GetRooms RoomManager::getRooms()
{
	Responses::GetRooms v;
	for (auto const& room : this->m_rooms)
	{
		Room oneRoom = room.second;
		Responses::RoomSendibleData tempRoom;
		tempRoom.roomName = oneRoom.m_metadata.roomName;
		tempRoom.hasPass = !oneRoom.m_metadata.password.empty();
		v.roomNames.push_back(tempRoom);
	}

	return v;
}