import sqlite3
import requests
from dataclasses import dataclass
from typing import List

URL = r'https://opentdb.com/api.php?type=multiple&amount=%d'
SQL_PATH = r'MyDB.sqlite'
TABLE_INIT = """CREATE TABLE IF NOT EXISTS USERS(ID INTEGER PRIMARY KEY, NAME TEXT, EMAIL TEXT, PASSWORD TEXT, SALT TEXT); 
	CREATE TABLE IF NOT EXISTS TRIVIAS(ID INTEGER PRIMARY KEY, CATEGORY TEXT, DIFFICULTY TEXT, QUESTION TEXT, OWNER INTEGER, FOREIGN KEY (OWNER) REFERENCES USERS(ID)); 
	CREATE TABLE IF NOT EXISTS ANSWERS(ID INTEGER PRIMARY KEY, ANSWER TEXT, IS_TRUE INTEGER, TRIVIA_ID INTEGER, FOREIGN KEY (TRIVIA_ID) REFERENCES TRIVIAS(ID)); 
	CREATE TABLE IF NOT EXISTS GAMES(ID INTEGER PRIMARY KEY); 
	CREATE TABLE IF NOT EXISTS SCORES(ID INTEGER PRIMARY KEY, SCORE REAL, USER_ID INTEGER, GAME_ID INTEGER, FOREIGN KEY (USER_ID) REFERENCES USERS(ID), FOREIGN KEY (GAME_ID) REFERENCES GAMES(ID));   
	CREATE TABLE IF NOT EXISTS SUBMITS(ID INTEGER PRIMARY KEY, RESPONSE_TIME REAL, ANSWERS_ID INTEGER, TRIVIAS_ID INTEGER, SCORE_ID INTEGER, FOREIGN KEY (SCORE_ID) REFERENCES SCORES(ID), FOREIGN KEY (TRIVIAS_ID) REFERENCES TRIVIAS(ID), FOREIGN KEY (ANSWERS_ID) REFERENCES ANSWERS(ID));"""

@dataclass
class Trivia:
    category: str
    difficulty: str
    question: str
    correct_answer: str
    incorrect_answers: List[str]

    def __init__(self, result):
        """
        :param request: what get form the server
        expmple: {'category': 'Politics', 'type': 'multiple', 'difficulty': 'hard', 'question': 'Who was the longest-serving senator in US history, serving from 1959 to 2010?', 'correct_answer': 'Robert Byrd', 'incorrect_answers': ['Daniel Inouye', 'Strom Thurmond', 'Joe Biden']}
        """
        self.add_field(result, 'category')
        self.add_field(result, 'difficulty')
        self.add_field(result, 'question')
        self.add_field(result, 'correct_answer')
        self.add_field(result, 'incorrect_answers')

    def add_field(self, json, field_name: str):
        setattr(self, field_name, json[field_name])


def request_trivias(url) -> List[Trivia]:
    resp = requests.get(url=url)
    json = resp.json()  # Check the JSON Response Content documentation below
    return [Trivia(x) for x in json['results']]


class TriviaDB:
    def __init__(self, path):
        conn = sqlite3.connect(path)
        self.conn = conn
        cur = self.conn.cursor()
        for command in TABLE_INIT.split(";"):
            cur.execute(command)

    def __del__(self):
        self.conn.close()

    def add_trivia_class(self, trivia: Trivia):
        trivia_id = self._add_trivia(trivia)
        self._add_questions(trivia, trivia_id)

    def _add_trivia(self, trivia: Trivia) -> int:
        sql = ''' INSERT INTO TRIVIAS(CATEGORY ,DIFFICULTY ,QUESTION)
              VALUES(?,?,?) '''
        cur = self.conn.cursor()
        cur.execute(sql, (trivia.category, trivia.difficulty, trivia.question))
        self.conn.commit()

        return cur.lastrowid

    def _add_questions(self, questions: Trivia, trivia_id: int):
        """ANSWER TEXT, IS_TRUE INTEGER, TRIVIA_ID INTEGER"""

        sql = ''' INSERT INTO ANSWERS(ANSWER,IS_TRUE,TRIVIA_ID)
                  VALUES(?,?,?) '''
        cur = self.conn.cursor()

        cur.execute(sql, (questions.correct_answer, 1, trivia_id))
        self.conn.commit()

        for incorrect_answer in questions.incorrect_answers:
            cur.execute(sql, (incorrect_answer, 0, trivia_id))


def main():
    num_of_start_q = 0
    try:
        num_of_start_q = int(input("enter the number of questions: "))
    except:
        print("can't handle the input")
        return

    db = TriviaDB(SQL_PATH)
    print("getting questions...")
    for trivia in request_trivias(URL % num_of_start_q):
        db.add_trivia_class(trivia)
    print("DONE!")


if __name__ == '__main__':
    main()
