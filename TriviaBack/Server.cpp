#include "Server.h"

Server::Server() : m_communicator(Communicator::getInstance())
{
}

Server::~Server()
{
}

void Server::run()
{
	this->m_communicator.startHandleRequests();
}