#include "Room.h"

Room::Room(Requests::CreateRoomReq rData)
{
	this->m_metadata = rData;
	this->isActive = 1;
}

bool Room::addUser(LoggedUser l)
{
	bool isFullRoom = this->m_users.size() >= this->m_metadata.maxUsers;
	if (isFullRoom)
	{
		return false;
	}
	if (playerInRoom(l))
	{
		return false;
	}

	this->m_users.push_back(l);
	return true;
}

bool Room::removeUser(LoggedUser l)
{
	if (!playerInRoom(l))
	{
		return false;
	}
	this->m_users.erase(std::remove(this->m_users.begin(), this->m_users.end(), l), this->m_users.end());
	return true;
}

vector<LoggedUser> Room::getAllUsers()
{
	return this->m_users;
}

Room Room::operator=(const Room& copy)
{
	this->m_metadata = copy.m_metadata;
	this->isActive = copy.isActive;
	this->m_blackListUsers = copy.m_blackListUsers;
	this->m_users = copy.m_users;
	return *this;
}

bool Room::isUserBaned(LoggedUser l)
{
	auto& blackList = this->m_blackListUsers;
	return blackList.find(l) != blackList.end();
}

bool Room::playerInRoom(LoggedUser l)
{
	return std::find(this->m_users.begin(), this->m_users.end(), l) != this->m_users.end();;
}