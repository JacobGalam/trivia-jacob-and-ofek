#include "MenuRequestHandler.h"

using namespace MenuOptions;

MenuRequestHandler::MenuRequestHandler(RoomManager& rManager, TriviaManager& triviaManager, StatisticsManager& sManager, RequestHandlerFactory& rHandlerFactory, string userName)
	:
	m_roomManager(rManager), triviaManager(triviaManager), m_statisticsManager(sManager), m_handlerFactory(rHandlerFactory)
{
	this->m_user = LoggedUser(userName);
}

void MenuRequestHandler::onExit()
{
	this->m_handlerFactory.getLoginManager().logoutUser(this->m_user.getUsername());
}

handleRequestFunc_map& MenuRequestHandler::optionsMap()
{
	static handleRequestFunc_map returnMap = {
		{Codes::signout, &MenuRequestHandler::signout},
		{Codes::getRooms, &MenuRequestHandler::getRooms},
		{Codes::getPlayersInRoom, &MenuRequestHandler::getPlayersInRoom},
		{Codes::getTops, &MenuRequestHandler::getTops},
		{Codes::getUserStatistics, &MenuRequestHandler::getUserStatistics},
		{Codes::joinRoom, &MenuRequestHandler::joinRoom},
		{Codes::createRoom, &MenuRequestHandler::createRoom},
		{Codes::addQuestion, &MenuRequestHandler::addQuestion},
		{Codes::removeQuestion, &MenuRequestHandler::removeQuestion},
		{Codes::getCategories, &MenuRequestHandler::getCategories},
		{Codes::getUserQuestions, &MenuRequestHandler::getUserQuestions},
	};

	return returnMap;
}

RequestResult MenuRequestHandler::signout(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;
	Responses::Logout returnAns;

	returnAns.status = mySelf->m_handlerFactory.getLoginManager().logoutUser(mySelf->m_user.getUsername());

	rR.response = JsonResponsePacketSerializer::serializeResponse(returnAns);
	rR.newHandler = mySelf->m_handlerFactory.createLoginRequestHandler();

	return rR;
}

RequestResult MenuRequestHandler::getRooms(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	Responses::GetRooms getRoom = mySelf->m_roomManager.getRooms();

	rR.response = JsonResponsePacketSerializer::serializeResponse(getRoom);
	return rR;
}

RequestResult MenuRequestHandler::getPlayersInRoom(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	auto roomData = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomReqRequest(rInfo.buffer);

	InputCheck::NotZeroAndMax(roomData.roomName, 20, "room name");

	Responses::GetPlayersInRoom getRoom;
	auto room = mySelf->m_roomManager.getRoomState(roomData.roomName);
	for (auto player : room.getAllUsers())
	{
		getRoom.players.push_back(player.getUsername());
	}
	getRoom.roomName = room.m_metadata.roomName;
	getRoom.category = room.m_metadata.category;
	rR.response = JsonResponsePacketSerializer::serializeResponse(getRoom);
	return rR;
}

RequestResult MenuRequestHandler::getUserStatistics(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	auto userStatsR = JsonRequestPacketDeserializer::deserializeUserStatsReqRequest(rInfo.buffer);

	InputCheck::MinMax(userStatsR.userName, 3, 20, "wanted username");

	Responses::UserStats userStats = mySelf->m_statisticsManager.getUserStatistics(userStatsR.userName);
	userStats.userName = userStatsR.userName;
	rR.response = JsonResponsePacketSerializer::serializeResponse(userStats);
	return rR;
}

RequestResult MenuRequestHandler::getTops(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	auto roomData = JsonRequestPacketDeserializer::deserializeTopReqRequest(rInfo.buffer);

	InputCheck::MinMax(roomData.maxUsers, 3, 20, "max players scores");

	Responses::Tops userStats = mySelf->m_statisticsManager.getHighScore(roomData.maxUsers);

	rR.response = JsonResponsePacketSerializer::serializeResponse(userStats);
	return rR;
}

RequestResult MenuRequestHandler::joinRoom(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	auto roomData = JsonRequestPacketDeserializer::deserializeJoinRoomReqRequest(rInfo.buffer);

	InputCheck::MinMax(roomData.password, -1, 20, "password");
	InputCheck::MinMax(roomData.roomName, 3, 20, "room name");

	Responses::UserJoin joined;
	joined.username = mySelf->m_user.getUsername();
	Responses::JoinRoom getRoom;
	auto& room = mySelf->m_roomManager.getRoomState(roomData.roomName);

	if (room.isUserBaned(mySelf->m_user))
	{
		throw std::exception("You were banned from this room!");
	}
	else if (!room.m_metadata.password.empty() && room.m_metadata.password != roomData.password)
	{
		throw std::exception("Wrong room password!");
	}
	else
	{
		getRoom.status = room.addUser(mySelf->m_user);
		getRoom.roomData = room.m_metadata;

		for (LoggedUser user : room.getAllUsers())
		{
			Communicator::getInstance().sendResponse(user, joined);
			getRoom.players.push_back(user.getUsername());
		}

		rR.newHandler = (IRequestHandler*)mySelf->m_handlerFactory.createRoomMemberRequestHandler(room, mySelf->m_user);

		rR.response = JsonResponsePacketSerializer::serializeResponse(getRoom);
	}

	return rR;
}

RequestResult MenuRequestHandler::createRoom(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	auto roomData = JsonRequestPacketDeserializer::deserializeCreateRoomReqRequest(rInfo.buffer);

	InputCheck::NotZeroAndMax(roomData.roomName, 20, "room name");
	InputCheck::NotZeroAndMax(roomData.category, 50, "category");
	InputCheck::MinMax(roomData.answerTimeout, 2, 50, "time per answer");
	InputCheck::NotZeroAndMax(roomData.answerTimeout, 20, "number of users in room");
	InputCheck::MinMax(roomData.password, -1, 20, "password"); // can be zero
	InputCheck::NotZeroAndMax(roomData.questionCount, 30, "number of questions");  // default max size

	int maxNumOfQuestions = mySelf->triviaManager.getNumOfCategoryQuestions(roomData.category);
	InputCheck::NotZeroAndMax(roomData.questionCount, maxNumOfQuestions, "number of questions");  // max size in db

	Responses::CreateRoom createRoomRespone;

	createRoomRespone.roomData.password = roomData.password;
	createRoomRespone.status = true;
	auto& room = mySelf->m_roomManager.createRoom(mySelf->m_user, roomData);
	createRoomRespone.roomData = roomData;
	createRoomRespone.status = 1;
	rR.response = JsonResponsePacketSerializer::serializeResponse(createRoomRespone);

	rR.newHandler = (IRequestHandler*)mySelf->m_handlerFactory.createRoomAdminRequestHandler(room, mySelf->m_user);

	return rR;
}

RequestResult MenuRequestHandler::addQuestion(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	auto questionData = JsonRequestPacketDeserializer::deserializeAddQuestionReqRequest(rInfo.buffer);

	InputCheck::NotZeroAndMax(questionData.category, 30, "category");
	InputCheck::NotZeroAndMax(questionData.question, 30, "question");
	InputCheck::NotZeroAndMax(questionData.rightAnswer, 30, "right answer");
	InputCheck::NotZeroAndMax(questionData.diff, 30, "difficulty");
	InputCheck::NotZeroAndMax(questionData.wrongAnswers[0], 30, "first wrong answers");
	InputCheck::NotZeroAndMax(questionData.wrongAnswers[1], 30, "scened wrong answers");
	InputCheck::NotZeroAndMax(questionData.wrongAnswers[2], 30, "third wrong answers");

	Responses::AddQuestion addQuestionResponse;

	mySelf->triviaManager.addTrivia(questionData.question, questionData.rightAnswer, questionData.wrongAnswers, questionData.category, questionData.diff, mySelf->m_user.getUsername());
	addQuestionResponse.status = 1;
	rR.response = JsonResponsePacketSerializer::serializeResponse(addQuestionResponse);

	return rR;
}

RequestResult MenuRequestHandler::removeQuestion(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	auto questionData = JsonRequestPacketDeserializer::deserializeRemoveQuestionReqRequest(rInfo.buffer);

	InputCheck::NotZeroAndMax(questionData.questionToRemove, 30, "question to remove");

	Responses::RemoveQuestion removeQuestionResponse;

	mySelf->triviaManager.removeTrivia(questionData.questionToRemove, mySelf->m_user.getUsername());

	removeQuestionResponse.status = 1;
	removeQuestionResponse.question = questionData.questionToRemove;

	rR.response = JsonResponsePacketSerializer::serializeResponse(removeQuestionResponse);

	return rR;
}

RequestResult MenuRequestHandler::getCategories(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	Responses::GetAllCategories getCategories;
	getCategories.categories = mySelf->triviaManager.getCategories();
	rR.response = JsonResponsePacketSerializer::serializeResponse(getCategories);

	return rR;
}

RequestResult MenuRequestHandler::getUserQuestions(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRequestHandler* mySelf = (MenuRequestHandler*)self;

	RequestResult rR;

	Responses::GetUserQuestions userQuestions;

	userQuestions.questions = mySelf->triviaManager.getUserQuestions(mySelf->m_user.getUsername());

	rR.response = JsonResponsePacketSerializer::serializeResponse(userQuestions);

	return rR;
}