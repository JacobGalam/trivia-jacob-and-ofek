#pragma once

#include "RequestHandlerFactory.h"
#include "DefaultMemberRequstHandler.h"

namespace RoomAdminOptions
{
	enum Codes
	{
		closeRoom = 0,
		startGame,
		kickUser,
		banUser,
	};
};

class RoomAdminRequestHandler : public DefaultMemberRequstHandler
{
public:
	/*
	c'tor that sets the fields roomName, m_user, m_roomManager and m_handlerFactory by the params
	input: string roomName, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory
	output: none
	*/
	RoomAdminRequestHandler(string roomName, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory);

	/*
	a function map that indicates the index of every method in the class by the needed request.
	input: none
	output: handleRequestFunc_map&: the map of the methods in the class
	*/
	virtual handleRequestFunc_map& optionsMap() override;

	virtual void onExit() override;
private:
	/*
	deserializes closeRoom req. removes all of the users from th room, sends them response and deletes the room. sends response
	that validates the closing.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static closeRoom(IRequestHandler* self, RequestInfo rInfo);
	/*
	deserializes startGame req. sends responses to all of the players that the game has started and it sends the first question.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static startGame(IRequestHandler* self, RequestInfo rInfo);
	RequestResult static kickUser(IRequestHandler* self, RequestInfo rInfo); //*BONUS*
	RequestResult static banUser(IRequestHandler* self, RequestInfo rInfo); //*BONUS*
};
