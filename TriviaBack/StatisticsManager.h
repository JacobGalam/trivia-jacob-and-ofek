#pragma once
#include <string>
#include <vector>
#include <mutex>

#include "SqlDataBase.h"
#include "Responses.h"

using std::string;
using std::vector;
using Responses::UserStats;
using Responses::Tops;

class StatisticsManager
{
public:
	StatisticsManager() = default; //c'tor
	/*
	c'tor that sets db by param
	input: IDatabase* iDataBase
	output: none
	*/
	StatisticsManager(IDatabase* iDataBase);
	/*
	returns a response of top high scores by the number of top players to get (usually 3) (uses db)
	input: int numOfTops - num of top players
	output: Responses::Top: top players data
	*/
	Responses::Tops getHighScore(int numOfTops);
	/*
	returns the response stats of specific user by its username (uses db)
	input: string username - the user to get data from
	output: Responses::UserStats: user stats
	*/
	Responses::UserStats getUserStatistics(string username);
	/*
	puts the final stats after the game finished by using the finishGame method from the db
	input: string username, int gameId, float score, std::vector<Answer> answers
	output: none
	*/
	void postStatistics(string username, int gameId, float score, std::vector<Answer> answers);
private:
	IDatabase* m_database;
	std::mutex m_managerMutex;
};
