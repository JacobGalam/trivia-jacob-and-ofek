#pragma once
#include <thread>
#include <chrono>
#include <algorithm>
#include <cstdlib>      // std::rand, std::srand
#include <random>
#include <time.h>
#include <set>
#include <map>
#include <mutex>

#include "Room.h"
#include "Trivia.h"
#include "IDatabase.h"

//#include "Communicator.h"
class Communicator;

struct UserGameStats
{
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
	float score;

	bool operator==(const UserGameStats& stats)
	{
		return this->correctAnswerCount == stats.correctAnswerCount && this->averageAnswerTime == stats.averageAnswerTime && this->wrongAnswerCount == stats.wrongAnswerCount && this->score == stats.score;
	}
};

class Game
{
public:
	Game() = default;
	//Game& operator=(const Game& game);
	Game(Room& room, std::vector<RoundData> rounds, unsigned int gameId);
	bool submitAnswer(LoggedUser user, string answer, time_t receivalTime);
	bool removePlayer(LoggedUser user);

	void startHandlingGame();
	void sendQuestion();
	void revealCorrectAnswer();
	void sendGameOver();
	void startGame();

	unsigned int getGameId();
	std::map<LoggedUser, UserGameStats> getPlayers();

private:
	std::mutex m_managerMutex;
	bool m_canGetAnswers;
	Requests::CreateRoomReq m_metadata;
	std::vector<RoundData> m_questions;
	std::map<LoggedUser, UserGameStats> m_players;
	unsigned int m_currentQuestion;
	time_t m_receivalTime;
	std::set<LoggedUser> m_answeredThisRound;
	unsigned int gameId;
	std::map<LoggedUser, std::vector<Answer>> allAnswers;
};

//#include "Communicator.h"