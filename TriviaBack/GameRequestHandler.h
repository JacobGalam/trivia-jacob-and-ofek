#pragma once
#include "IRequestHandler.h"
#include "Game.h"
#include "GameManager.h"
#include "RequestHandlerFactory.h"
#include "SendMsgRequestHandler.h"

/*
Options
*/
namespace GameOptions
{
	enum Codes
	{
		submitAnswer,
		leaveGame,
	};
}

class GameRequestHandler : public SendMsgRequestHandler
{
public:
	/*
	Ctor of GameRequestHandler
	Input: game user gameManager handlerFactory
	Output: None
	*/
	GameRequestHandler(Game& game, LoggedUser user, GameManager& gameManager, RequestHandlerFactory& handlerFactory);

	/*
	Send the users the msg
	Input: This class, the message and who send
	Output: None
	*/
	static void sendGameMessage(IRequestHandler* self, Responses::SendMessageChat msg, LoggedUser sender);

	/*
	what happen when user exit
	Input: None
	Output: None
	*/
	virtual void onExit() override;

	/*
	Get options of this handler
	Input: None
	Output: option map
	*/
	virtual handleRequestFunc_map& optionsMap() override;
private:
	Game& m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFactory;

	/*
	submit Answer
	Input: this class and info about the Request
	Output: what to send and the new handler if needed
	*/
	static RequestResult submitAnswer(IRequestHandler* self, RequestInfo rInfo);
	/*
	leave Game
	Input: this class and info about the Request
	Output: what to send and the new handler if needed
	*/
	static RequestResult leaveGame(IRequestHandler* self, RequestInfo rInfo);

	/*
	Send data to users
	Input: what to send and the except user
	Output: void
	*/
	template <class MsgType>
	void sendMsgToUsers(MsgType msg, LoggedUser except)
	{
		for (auto const& LoggedUser : this->m_game.getPlayers())
		{
			if (LoggedUser.first.getUsername() == except.getUsername())
			{
				continue;
			}
			Communicator::getInstance().sendResponse(LoggedUser.first, msg);
		}
	}
};
