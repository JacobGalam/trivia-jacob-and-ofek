#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <type_traits>

#include "sqlite3.h"

using std::string;
using Record = std::vector<string>;
using Records = std::vector<Record>;

// todo make the string ref to prevent the coping

class SqlDataBaseCore
{
public:
	bool open(string path, const char* dataInit);
	void close();
	/*
	Return the answers of DB
	Input: The commend to the DB
	Output: The return Records
	*/
	virtual Records select(string comment);

	virtual string selectValue(string comment);
	virtual int selectInt(string comment);
	virtual int lastInsertId();
	virtual float selectFloat(string comment);

	virtual Record selectRow(string comment);

	Record selectColumn(string comment, int index = 0);

	/*
	Run the comment and check for error
	Input: The comment
	Output: The return int of exec
	*/
	const int exce(string comment);

	template<class Type>
	static string Commmnad(Type value)
	{
		if (std::is_integral<Type>::value)
		{
			return std::to_string(value);
		}
		throw std::exception("Can not make the type to string");
	}

	// use as static class or namespace
	template< typename FIRST, typename... REST >
	static string Commmnad(const FIRST first, const REST... rest)
	{
		return SqlDataBaseCore::Commmnad(first) + " " + SqlDataBaseCore::Commmnad(rest...);
	}

	static string AddToList();

	template< typename type >
	static string AddToList(type value)
	{
		return SqlDataBaseCore::Commmnad(value);
	}

	template< typename FIRST, typename... REST >
	static string AddToList(const FIRST first, const REST... rest)
	{
		return SqlDataBaseCore::AddToList(first) + ", " + SqlDataBaseCore::AddToList(rest...);
	}

	template< typename FIRST, typename... REST >
	static string StartList(const FIRST first, const REST... rest)
	{
		return "(" + SqlDataBaseCore::AddToList(first, rest...) + ")";
	}
	/*
	end of commmnad
	*/
	static string Commmnad();

	/*
	a text, add '
	*/
	static string Commmnad(const string data);
	static string Commmnad(const float data);

	/*
	a int
	*/
	static string Commmnad(const int data);

	/*
	command. A regaler command text without '
	*/
	static string Commmnad(const char* data);

private:
	/*
	Check if have error and print it
	Input: The comment and the return error message
	Output: None
	*/
	static void checkError(const char* comment, char* errMessage);
	/*
	Save return answers from DB to the first pointer, use for execSQLlite3
	Input: First value is Records pointer... input of execSQLlite3
	Output: 0 if not fail 1 else
	*/
	static int selectCallback(void* pData, int numFields, char** pFields, char** pColNames);

public:
	// HELPERS

	/*
	Insert the data one time to the SQL
	Input: db, tableName, colName and the data
	Output: None
	*/
	void smartInsert(string tableName, string col, string data);
	/*
	Update the value to new value
	Input: Table and col name, old value and the new value
	Output: None
	*/
	void updateValue(string table, string col, string oldValue, string newValue);
	/*
	Delete from the DB
	Input: Table name and where comment
	Output: None
	*/
	void deleteDataW(string table, string whereQ);

	/*
	Get data from the DB
	Input: which table to search, what to get(optional), and condition(optional)
	Output: The return Records
	*/
	virtual Records smartGet(string table, string col, string whereQ);

private:
	sqlite3* _db;
};

#define SQL_COMMAND SqlDataBaseCore::Commmnad
#define SQL_LIST(...) SqlDataBaseCore::StartList(__VA_ARGS__).c_str()