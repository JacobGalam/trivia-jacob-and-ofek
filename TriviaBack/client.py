import socket
import binascii
import array
import json

IP = "127.0.0.1"
PORT = 8826

LOGIN = dict(username="", password="")

SIGNUP = dict(username="", password="", email="")


def server_connect():
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.connect((IP, PORT))
    return soc


def get_msg(code: int, msg: dict) -> bytearray:
    to_send = bytearray()

    to_send.append(code)
    jsonMsg = json.dumps(msg)
    print(type(jsonMsg))

    for x in [hex(len(str(jsonMsg)) >> i & 0xff) for i in (24,16,8,0)][::-1]:
        print(x)
        to_send.append(int(x, 16))

    for x in str(jsonMsg):
        to_send.append(ord(x))
    print(to_send)

    return to_send


def main():
    to_send = bytearray()

    try:
        soc = server_connect()

        while True:
            choice = int(input("1 for login, 2 for signup: "))

            if choice == 1:
                LOGIN['username'] = input("Username: ")
                LOGIN['password'] = input("Password: ")
                soc.sendall(get_msg(0, LOGIN))
            elif choice == 2:
                send_dict = {}
                send_dict['username'] = input("Username: ")
                send_dict['password'] = input("Password: ")
                send_dict['email'] = input("Email: ")
                print(send_dict)
                soc.sendall(get_msg(1, send_dict))

            reply = soc.recv(1024).decode()
            print(reply)

        #soc.close()
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()
