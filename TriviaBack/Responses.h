#pragma once
#include <string>

#include "Buffer.h"
#include "Sendible.h"
#include "Requests.h"

#include "nlohmann/json.hpp"

// for convenience
using json = nlohmann::json;

namespace Responses
{
	struct Login
	{
		unsigned int status = 0;
		constexpr static auto properties = std::make_tuple(
			PROPERTY(Login, status)
		);
	};

	struct Signup
	{
		unsigned int status = 0;
		unsigned int value = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(Signup, status),
			PROPERTY(Signup, value)
		);
	};

	struct Logout
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(Logout, status)
		);
	};

	struct RoomData
	{
		string name;
		int maxPlayers;
		unsigned int timePerQuestion;
		unsigned int isActive;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(RoomData, name),
			PROPERTY(RoomData, maxPlayers),
			PROPERTY(RoomData, timePerQuestion),
			PROPERTY(RoomData, isActive)
		);
	};

	struct RoomSendibleData
	{
		string roomName;
		bool hasPass;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(RoomSendibleData, roomName),
			PROPERTY(RoomSendibleData, hasPass)
		);
	};

	struct GetRooms
	{
		std::vector<RoomSendibleData> roomNames;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetRooms, roomNames)
		);
	};

	struct GetPlayersInRoom
	{
		std::vector<string> players;
		string roomName;
		string category;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetPlayersInRoom, players),
			PROPERTY(GetPlayersInRoom, roomName),
			PROPERTY(GetPlayersInRoom, category)
		);
	};

	struct JoinRoom
	{
		unsigned int status = 0;
		Requests::CreateRoomReq roomData;
		std::vector<string> players;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(JoinRoom, status),
			PROPERTY(JoinRoom, roomData),
			PROPERTY(JoinRoom, players)
		);
	};

	struct ExitRoom
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(ExitRoom, status)
		);
	};

	struct CreateRoom
	{
		unsigned int status = 0;
		Requests::CreateRoomReq roomData;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(CreateRoom, status),
			PROPERTY(CreateRoom, roomData)
		);
	};

	struct Score
	{
		string username;
		float score;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(Score, username),
			PROPERTY(Score, score)
		);
	};

	struct UserStats
	{
		string userName;
		unsigned int numOfGame = 0;
		unsigned int numWrongAns = 0;
		unsigned int numRightAns = 0;
		float avrageTimeToAns = 0;
		unsigned int numOfQuestions = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(UserStats, numOfGame),
			PROPERTY(UserStats, userName),
			PROPERTY(UserStats, numWrongAns),
			PROPERTY(UserStats, numRightAns),
			PROPERTY(UserStats, avrageTimeToAns),
			PROPERTY(UserStats, numOfQuestions)
		);
	};

	struct Tops
	{
		std::vector<Responses::Score> tops;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(Tops, tops)
		);
	};

	struct CloseRoom
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(CloseRoom, status)
		);
	};

	struct StartGame
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(StartGame, status)
		);
	};

	struct GetRoomState
	{
		unsigned int status = 0;
		bool hasGameBegun;
		std::vector<string> players;
		unsigned int questionCount = 0;
		unsigned int answerAnswer = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetRoomState, status),
			PROPERTY(GetRoomState, hasGameBegun),
			PROPERTY(GetRoomState, players),
			PROPERTY(GetRoomState, questionCount),
			PROPERTY(GetRoomState, answerAnswer)
		);
	};

	struct UserLeave
	{
		string username;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(UserLeave, username)
		);
	};

	struct UserJoin
	{
		string username;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(UserJoin, username)
		);
	};

	struct LeaveGame
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(LeaveGame, status)
		);
	};

	struct GetQuestion
	{
		unsigned int status = 0;
		string question;
		std::vector<string> answers;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetQuestion, status),
			PROPERTY(GetQuestion, question),
			PROPERTY(GetQuestion, answers)
		);
	};

	struct SubmitAnswer
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(SubmitAnswer, status)
		);
	};

	struct updateRightAnswer
	{
		float score;
		string rightAnswer;
		std::vector<Score> gameScores;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(updateRightAnswer, score),
			PROPERTY(updateRightAnswer, rightAnswer),
			PROPERTY(updateRightAnswer, gameScores)
		);
	};

	struct PlayerResults
	{
		string username;
		float finalResult = 0;
		unsigned int correctAnswerCount = 0;
		unsigned int wrongAnswerCount = 0;
		float averageAnswerTime = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(PlayerResults, username),
			PROPERTY(PlayerResults, correctAnswerCount),
			PROPERTY(PlayerResults, wrongAnswerCount),
			PROPERTY(PlayerResults, averageAnswerTime),
			PROPERTY(PlayerResults, finalResult)
		);
	};

	struct GetGameResults
	{
		unsigned int status = 0;
		std::vector<Responses::PlayerResults> results;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetGameResults, status),
			PROPERTY(GetGameResults, results)
		);
	};

	struct StartGamedata
	{
		std::vector<string> players;
		string gameName;
		int questionNum = 0;
		float timePerquestion = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(StartGamedata, gameName),
			PROPERTY(StartGamedata, players),
			PROPERTY(StartGamedata, questionNum),
			PROPERTY(StartGamedata, timePerquestion)
		);
	};

	struct SendMessageChat //bonus
	{
		string msg;
		string user;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(SendMessageChat, msg),
			PROPERTY(SendMessageChat, user)
		);
	};

	struct KickUser //bonus
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(KickUser, status)
		);
	};

	struct BanUser //bonus
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(BanUser, status)
		);
	};

	struct AddQuestion //bonus
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(AddQuestion, status)
		);
	};

	struct RemoveQuestion //bonus
	{
		string question = "";
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(RemoveQuestion, question),
			PROPERTY(RemoveQuestion, status)
		);
	};

	struct GetAllCategories //bonus
	{
		std::vector<string> categories;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetAllCategories, categories)
		);
	};

	struct GetUserQuestions //bonus
	{
		std::vector<string> questions;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetUserQuestions, questions)
		);
	};

	struct Error
	{
		string message;
		bool close = false;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(Error, message),
			PROPERTY(Error, close)
		);
	};
}
