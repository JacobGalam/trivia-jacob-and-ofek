#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "InputCheck.h"
#include "TriviaManager.h"
#include  "Communicator.h"

namespace MenuOptions
{
	enum Codes
	{
		signout,
		getRooms,
		getPlayersInRoom,
		getTops,
		getUserStatistics,
		joinRoom,
		createRoom,
		addQuestion,
		removeQuestion,
		getCategories,
		getUserQuestions,
	};
}

class MenuRequestHandler : public IRequestHandler
{
public:
	/*
	c'tor that sets the fields: RoomManager, StatisticsManager, RequestHandlerFactory and userName
	input: RoomManager& rManager, StatisticsManager& sManager, RequestHandlerFactory& rHandlerFactory, string userName
	output: none
	*/
	MenuRequestHandler(RoomManager& rManager, TriviaManager& triviaManager, StatisticsManager& sManager, RequestHandlerFactory& rHandlerFactory, string userName);

private:
	virtual void onExit() override;

	LoggedUser m_user;
	RoomManager& m_roomManager;
	TriviaManager& triviaManager;
	StatisticsManager& m_statisticsManager;
	RequestHandlerFactory& m_handlerFactory;

	/*
	deserializes signout req from the user, removes it from the logins in the login manager and serializes signout response
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static signout(IRequestHandler* self, RequestInfo rInfo);
	/*
	deserializes getRooms req from user in order to show the available rooms. serializes response by getting the rooms from the
	room manager.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static getRooms(IRequestHandler* self, RequestInfo rInfo);
	/*
	deserializes getPlayersInRoom req that includes the room and "scans" the players in the room by the room manager.
	serializes a response that includes vector of the players.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static getPlayersInRoom(IRequestHandler* self, RequestInfo rInfo);
	/*
	deserializes getUserStatistics req and gets the player's stats by using the statistics manager. serializes the response that
	includes the stats;
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static getUserStatistics(IRequestHandler* self, RequestInfo rInfo);
	/*
	deserializes getTops req, uses the statistics manager for getting the high scores by the number of top players requested (usually 3).
	serializes a response with the top players
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static getTops(IRequestHandler* self, RequestInfo rInfo);
	/*
	deserializes joinRoom req. gets the room state, checks if its isn't full and adds the user to it. In the end' it updates
	the other players in the room the the user joined. It serializes a response of joining room
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static joinRoom(IRequestHandler* self, RequestInfo rInfo);
	/*
	deserializes createRoom req, validates the req and creates a room by the RoomManager and serializes a response.
	if failed, it throws error.
	input: IRequestHandler* self - the current handler for the user, RequestInfo rInfo - the request sent by the user
	output: RequestResult: the result struct of the serialized response and next handler
	*/
	RequestResult static createRoom(IRequestHandler* self, RequestInfo rInfo);
	RequestResult static addQuestion(IRequestHandler* self, RequestInfo rInfo); //*BONUS*
	RequestResult static removeQuestion(IRequestHandler* self, RequestInfo rInfo); //*BONUS*
	RequestResult static getCategories(IRequestHandler* self, RequestInfo rInfo); //*BONUS*
	RequestResult static getUserQuestions(IRequestHandler* self, RequestInfo rInfo); //*BONUS*

	// Inherited via IRequestHandler
	virtual handleRequestFunc_map& optionsMap() override;
};
