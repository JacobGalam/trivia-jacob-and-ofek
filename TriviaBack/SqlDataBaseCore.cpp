#include "SqlDataBaseCore.h"

bool SqlDataBaseCore::open(string path, const char* dataInit)
{
	int res = sqlite3_open(path.c_str(), &this->_db);
	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB\n";
		return false;
	}

	char* errMessage = nullptr;

	sqlite3_exec(this->_db, dataInit, nullptr, nullptr, &errMessage);

	checkError(dataInit, errMessage);

	return true;
}

void SqlDataBaseCore::close()
{
	this->_db = nullptr;
	sqlite3_close(this->_db);
}

void SqlDataBaseCore::checkError(const char* comment, char* errMessage)
{
	if (errMessage)
	{
		std::cout << "the comment: " << '"' << comment << '"' << " cause the error: " << '"' << errMessage << '"' << "\n";
		sqlite3_free(errMessage);
	}
}

int SqlDataBaseCore::selectCallback(void* dataPointer, int numFields, char** pFields, char** pColNames)
{
	Records* records = static_cast<Records*>(dataPointer);  // casting from the void pointer to Records class
	try
	{
		records->emplace_back(pFields, pFields + numFields);  // add the data
	}
	catch (...)
	{
		// abort select on failure, don't let exception propogate thru sqlite3 call-stack
		return 1;
	}
	return 0;
}

Records SqlDataBaseCore::select(string comment)
{
	Records records;
	char* errorMsg;
	int ret = sqlite3_exec(this->_db, comment.c_str(), SqlDataBaseCore::selectCallback, &records, &errorMsg);
	if (ret != SQLITE_OK)
	{
		std::cerr << "Error in select statement " << comment << "[" << errorMsg << "]\n";
	}
	else
	{
		//std::cerr << records.size() << " records returned.\n";
	}

	return records;
}

string SqlDataBaseCore::selectValue(string comment)
{
	return this->select(comment)[0][0];
}

int SqlDataBaseCore::selectInt(string comment)
{
	return stoi(this->selectValue(comment));
}

int SqlDataBaseCore::lastInsertId()
{
	return sqlite3_last_insert_rowid(this->_db);
}

float SqlDataBaseCore::selectFloat(string comment)
{
	return stof(this->selectValue(comment));
}

Record SqlDataBaseCore::selectRow(string comment)
{
	return this->select(comment)[0];
}

Record SqlDataBaseCore::selectColumn(string comment, int index)
{
	Record columns;
	for (auto row : this->select(comment))
	{
		columns.push_back(row[index]);
	}
	return columns;
}

const int SqlDataBaseCore::exce(string comment)
{
	char* errMessage = nullptr;
	int returnINT = sqlite3_exec(this->_db, comment.c_str(), nullptr, nullptr, &errMessage);

	checkError(comment.c_str(), errMessage);

	return returnINT;
}

string SqlDataBaseCore::AddToList()
{
	return "";
}

string SqlDataBaseCore::Commmnad()
{
	return ";";
}

string SqlDataBaseCore::Commmnad(const string data)
{
	return "'" + string(data) + "'";
}

string SqlDataBaseCore::Commmnad(const float data)
{
	return std::to_string(data);
}

string SqlDataBaseCore::Commmnad(const int data)
{
	return std::to_string(data);
}

string SqlDataBaseCore::Commmnad(const char* data)
{
	return string(data);
}

void SqlDataBaseCore::smartInsert(string tableName, string col, string data)
{
	string comment = "INSERT INTO " + tableName + "(" + col + ") SELECT '" + data + "' WHERE NOT EXISTS(SELECT 1 FROM " + tableName + " WHERE " + col + " = '" + data + "');";
	this->exce(comment);
}

void SqlDataBaseCore::updateValue(string table, string col, string oldValue, string newValue)
{
	string comment = "UPDATE " + table + " set " + col + "='" + newValue + "' where " + col + "='" + oldValue + "';";
	this->exce(comment);
}

void SqlDataBaseCore::deleteDataW(string table, string whereQ)
{
	string comment = "DELETE FROM " + table + " where " + whereQ + ";";
	this->exce(comment);
}

Records SqlDataBaseCore::smartGet(string table, string col = "*", string whereQ = "1=1")
{
	auto Sqlcommend = "select " + col + " from " + table + " where " + whereQ + ";";
	return this->select(Sqlcommend);
}