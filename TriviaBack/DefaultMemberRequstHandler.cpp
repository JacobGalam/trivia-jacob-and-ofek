#include "DefaultMemberRequstHandler.h"

using namespace DefaultRoomOptions;

DefaultMemberRequstHandler::DefaultMemberRequstHandler(string roomName, LoggedUser user,
	RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory)
	:
	m_user(user), m_roomManager(roomManager), m_handlerFactory(requestHandlerFactory), SendMsgRequestHandler(&DefaultMemberRequstHandler::sendGameMessages, m_user)
{
	this->roomName = roomName;
}

void DefaultMemberRequstHandler::sendGameMessages(IRequestHandler* self, Responses::SendMessageChat msg, LoggedUser sender)
{
	DefaultMemberRequstHandler* mySelf = (DefaultMemberRequstHandler*)self;
	mySelf->sendMsgToUsers(msg, sender);
}

RequestResult DefaultMemberRequstHandler::getRoomState(IRequestHandler* self, RequestInfo rInfo)
{
	DefaultMemberRequstHandler* mySelf = (DefaultMemberRequstHandler*)self;

	RequestResult rR;
	auto roomState = JsonRequestPacketDeserializer::deserializeGetRoomStateReqRequest(rInfo.buffer);

	Responses::GetRoomState getRoomState;

	auto& roomData = mySelf->m_roomManager.getRoomState(mySelf->roomName);

	getRoomState.status = roomData.isActive;
	getRoomState.questionCount = roomData.m_metadata.questionCount;

	std::vector<string> players;
	for (LoggedUser user : roomData.getAllUsers())
	{
		players.push_back(user.getUsername());
	}

	getRoomState.players = players;
	getRoomState.hasGameBegun = roomData.isActive;
	getRoomState.answerAnswer = roomData.m_metadata.answerTimeout;

	rR.response = JsonResponsePacketSerializer::serializeResponse(getRoomState);
	return rR;
}

RequestResult DefaultMemberRequstHandler::getUserStatistics(IRequestHandler* self, RequestInfo rInfo)
{
	DefaultMemberRequstHandler* mySelf = (DefaultMemberRequstHandler*)self;

	RequestResult rR;

	auto userStatsR = JsonRequestPacketDeserializer::deserializeUserStatsReqRequest(rInfo.buffer);

	auto& statisticsManager = mySelf->m_handlerFactory.getStatisticsManager();

	Responses::UserStats userStats = statisticsManager.getUserStatistics(userStatsR.userName);
	userStats.userName = userStatsR.userName;
	rR.response = JsonResponsePacketSerializer::serializeResponse(userStats);
	return rR;
}

void DefaultMemberRequstHandler::onExit()
{
}

handleRequestFunc_map& DefaultMemberRequstHandler::optionsMap()
{
	static handleRequestFunc_map returnMap = SendMsgRequestHandler::optionsMap();
	static bool initMyFunctions = false;

	if (!initMyFunctions)
	{
		returnMap[Codes::getUserStatistics] = &DefaultMemberRequstHandler::getUserStatistics;

		initMyFunctions = true;
	}

	return returnMap;
}