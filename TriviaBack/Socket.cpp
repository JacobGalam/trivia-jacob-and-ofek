#include "Socket.h"

Socket::Socket(SOCKET sock)
{
	this->sock = sock;
}

Socket::~Socket()
{
	closesocket(this->sock);
}

void Socket::send(Buffer buff) const
{
	Helper::sendData(this->sock, string(buff.begin(), buff.end()));
}

Buffer Socket::receive() const
{
	unsigned char code = Helper::getIntPartFromSocket<unsigned char>(this->sock);
	unsigned int size = Helper::getIntPartFromSocket<unsigned int>(this->sock);
	auto msg = Helper::getStringPartFromSocket(this->sock, size);
	return Buffer().insertCode(code).insertNumber(size).insertStr(msg);
}