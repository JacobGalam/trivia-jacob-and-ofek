#pragma once
#include <vector>
#include <time.h>
#include "RequestResult.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "Socket.h"

struct RequestInfo
{
	time_t receivalTime;

	Buffer buffer;
	Socket* userSock;

	auto code()
	{
		return this->buffer.getCode();
	}

	explicit operator RequestResult() const
	{
		RequestResult reutn;
		reutn.response = this->buffer;
		return reutn;
	}
};

typedef RequestResult(*handleRequestFunc)(IRequestHandler* self, RequestInfo what);
typedef std::map<unsigned char, handleRequestFunc> handleRequestFunc_map;

class IRequestHandler
{
public:
	IRequestHandler()
	{
		time(&lastSendTime);
	}
	/*
	Handling the client's request by checking if the request includes the code of login or signup and creating a struct of the response.
	If the request is invalid, It created error response
	input: RequestInfo - struct that includes the request
	output: RequestResult - struct that includes the response
	*/
	virtual handleRequestFunc_map& optionsMap() = 0;
	virtual void onExit() = 0;

	RequestResult handleRequest(RequestInfo requestInfo)
	{
		lastSendTime = requestInfo.receivalTime;
		auto funcMap = this->optionsMap();
		auto code = requestInfo.buffer.getCode();
		auto func = funcMap.find(code);
		if (func == funcMap.end())
		{
			RequestResult respone;
			Error err;
			err.message = "Unknow code";
			respone.response = JsonResponsePacketSerializer::serializeResponse(err);
			return respone;
		}
		else
		{
			auto funcPointer = func->second;
			return funcPointer(this, requestInfo);
		}
	}
	time_t lastSendTime;
private:
};
