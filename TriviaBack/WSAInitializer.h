#pragma once

#include <WinSock2.h>
#include <Windows.h>

class WSAInitializer
{
public:
	/*
	Init the win sock. must be init
	input: None
	output: None
	*/
	WSAInitializer();

	/*
	Ctor the WSA
	input: None
	output: None
	*/
	~WSAInitializer();
};
