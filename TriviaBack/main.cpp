#pragma comment (lib, "ws2_32.lib")
#include <assert.h>     /* assert */
#include <algorithm>
#include <cctype>

#include "Server.h"
#include "SqlDataBase.h"

#define TEST false

using std::thread;

int main()
{
	WSAInitializer WSint = WSAInitializer();
	Server server;
	server.run();
	// start new therd for the server

	std::cout << "start server!\n";
	string input = "";
	while (input != "exit")
	{
		std::cin >> input;

		// to lower case
		std::transform(input.begin(), input.end(), input.begin(),
			[](unsigned char c) { return std::tolower(c); });

		std::cout << "unknow commend: '" << input << "'\n";
		// stuff
	}
}