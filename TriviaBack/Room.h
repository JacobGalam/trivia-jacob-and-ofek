#pragma once
#include <algorithm>
#include <string>
#include <vector>
#include <set>

#include "LoggedUser.h"
#include "Responses.h"
#include "Requests.h"
using std::string;
using std::vector;

using Responses::RoomData;

class Room
{
public:
	/*
	default c'tor for creating temp object.
	*/
	Room() = default;
	/*
	c'tor used for creating new rooms.
	input: RoomData rData - struct the includes the metadata to insert
	*/
	Room(RoomData rData);

	Room(Requests::CreateRoomReq rData);
	/*
	adding a new user to the vector of users.
	input: LoggedUser l - new user to add
	*/
	bool addUser(LoggedUser l);
	/*
	removing an user from the users vector
	input: LoggedUser l - the user to remove
	*/
	bool removeUser(LoggedUser l);
	/*
	getter for the m_users vector
	input: none
	output: the vector field of the users
	*/
	vector<LoggedUser> getAllUsers();
	/*
	getter for the metadata struct that includes the room data
	input: none
	output: the metadata struct of the current room
	*/

	Room operator=(const Room& copy);

	bool isUserBaned(LoggedUser l); //*BONUS*
private:
	bool playerInRoom(LoggedUser l);
public:
	Requests::CreateRoomReq m_metadata;
	int isActive;
	std::set<LoggedUser> m_blackListUsers;
private:
	vector<LoggedUser> m_users;
};
