#include "Buffer.h"

Buffer& Buffer::insertNumber(unsigned int enteredNum)
{
	for (int i = 0; i <= 24; i += 8)
	{
		this->push_back((enteredNum >> i) & 0xFF);
	}
	return *this;
}

Buffer& Buffer::insertStr(string enteredStr)
{
	for (auto ch : enteredStr)
	{
		this->push_back(ch);
	}
	return *this;
}

Buffer& Buffer::insertCode(unsigned char enteredStr)
{
	this->push_back(enteredStr);
	return *this;
}

json Buffer::getJson()
{
	string s(this->begin() + 5, this->end());
	return json::parse(s);
}

unsigned int Buffer::getSize()
{
	return *reinterpret_cast<unsigned int*>(&(*this)[1]);
}

unsigned char Buffer::getCode()
{
	auto arrayPointer = this->data();
	return *(unsigned char*)(arrayPointer);
}