#include "SqlDataBase.h"

// userName, string password, string email
#define DATABASE_INIT \
" \
	CREATE TABLE IF NOT EXISTS USERS(ID INTEGER PRIMARY KEY, NAME TEXT, EMAIL TEXT, PASSWORD TEXT, SALT TEXT); \
	CREATE TABLE IF NOT EXISTS TRIVIAS(ID INTEGER PRIMARY KEY, CATEGORY TEXT, DIFFICULTY TEXT, QUESTION TEXT, OWNER INTEGER, FOREIGN KEY (OWNER) REFERENCES USERS(ID)); \
	CREATE TABLE IF NOT EXISTS ANSWERS(ID INTEGER PRIMARY KEY, ANSWER TEXT, IS_TRUE INTEGER, TRIVIA_ID INTEGER, FOREIGN KEY (TRIVIA_ID) REFERENCES TRIVIAS(ID)); \
	CREATE TABLE IF NOT EXISTS GAMES(ID INTEGER PRIMARY KEY); \
	CREATE TABLE IF NOT EXISTS SCORES(ID INTEGER PRIMARY KEY, SCORE REAL, USER_ID INTEGER, GAME_ID INTEGER, FOREIGN KEY (USER_ID) REFERENCES USERS(ID), FOREIGN KEY (GAME_ID) REFERENCES GAMES(ID));   \
	CREATE TABLE IF NOT EXISTS SUBMITS(ID INTEGER PRIMARY KEY, RESPONSE_TIME REAL, ANSWERS_ID INTEGER, TRIVIAS_ID INTEGER, SCORE_ID INTEGER, FOREIGN KEY (SCORE_ID) REFERENCES SCORES(ID), FOREIGN KEY (TRIVIAS_ID) REFERENCES TRIVIAS(ID), FOREIGN KEY (ANSWERS_ID) REFERENCES ANSWERS(ID));"

SqlDataBase::SqlDataBase()
{
	if (!open())
	{
		throw std::exception("cant start the server");
	}
}

bool SqlDataBase::open()
{
	//return this->core.open("MyDB.sqlite", DATABASE_INIT);
	return this->core.open("MyDB.sqlite", ";");
}

bool SqlDataBase::clearAllUsersData()
{
	if (_DEBUG)
	{
		std::cout << "clears tables!\n";
		auto commands = this->core.selectColumn("select 'drop table ' || name || ';' from sqlite_master where type = 'table' and name != 'TRIVIAS' and name != 'ANSWERS';", 0);
		for (auto command : commands)
			this->core.exce(command);
		return true;
	}
	else
	{
		return false;
	}
}

bool SqlDataBase::doesUserExist(string userName)
{
	auto ans = this->core.smartGet("USERS", "*", "name='" + userName + "'");
	return !(ans.empty());
}

bool SqlDataBase::doesTriviaExist(string question, string username)
{
	auto command = SQL_COMMAND("SELECT * FROM TRIVIAS WHERE QUESTION=", question, "AND OWNER=", this->getUserId(username));
	return !(this->core.select(command).empty());
}

bool SqlDataBase::doesCategoryExist(string category)
{
	auto ans = this->core.smartGet("CATEGORIES", "*", "CATEGORY='" + category + "'");
	return !(ans.empty());
}

RoundData SqlDataBase::getTrivia(Record triviaRecord)
{
	RoundData returnTrivia;

	int id = stoi(triviaRecord[0]);
	returnTrivia.id = id;
	returnTrivia.category = triviaRecord[1];
	returnTrivia.difficulty = triviaRecord[2];
	returnTrivia.question = triviaRecord[3];
	auto command = SQL_COMMAND("select * from ANSWERS where TRIVIA_ID=", id);

	for (auto question : this->core.select(command))
	{
		int isTheAns = stoi(question[2]);
		auto questionText = question[1];
		if (isTheAns)
		{
			returnTrivia.correctAnswer = questionText;
		}
		else
		{
			returnTrivia.incorrectAnswers.push_back(questionText);
		}
	}

	return returnTrivia;
}

std::string SqlDataBase::randomSalt(size_t length)
{
	auto randchar = []() -> char
	{
		const char charset[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";
		const size_t max_index = (sizeof(charset) - 1);
		return charset[rand() % max_index];
	};
	std::string str(length, 0);
	std::generate_n(str.begin(), length, randchar);
	return str;
}

void SqlDataBase::close()
{
	this->core.close();
}

unsigned long long SqlDataBase::getPlayerTotalScore(string userName)
{
	int score = 0;
	for (auto game : this->getScores(userName))
	{
		score += stoi(game[1]);
	}
	return score;
}

std::vector<Responses::Score> SqlDataBase::getTops(int numOfTops)
{
	std::vector<Responses::Score> returnVector;
	returnVector.reserve(numOfTops);

	for (auto userName : this->core.selectColumn(SQL_COMMAND("SELECT * FROM USERS"), 1))
	{
		Responses::Score score;
		score.username = userName;
		score.score = this->getPlayerTotalScore(userName);
		returnVector.push_back(score);
		// DO THIS
	}
	std::sort(returnVector.begin(), returnVector.end(),
		[](Responses::Score a, Responses::Score b) {return a.score > b.score; });
	if (returnVector.size() < numOfTops) throw std::exception("Not enough players for top");
	returnVector.resize(numOfTops);
	return returnVector;
}

std::vector<string> SqlDataBase::getCategories()
{
	auto& core = this->core;
	auto command = SQL_COMMAND("SELECT DISTINCT CATEGORY FROM TRIVIAS");

	return core.selectColumn(command);
}

std::vector<string> SqlDataBase::getUserQuestions(string username)
{
	auto& core = this->core;
	auto command = SQL_COMMAND("SELECT QUESTION FROM TRIVIAS WHERE OWNER=", this->getUserId(username));

	return core.selectColumn(command);
}

int SqlDataBase::getNumOfQuestions(string username)
{
	return this->core.selectInt(SQL_COMMAND("SELECT COUNT(ID) FROM TRIVIAS WHERE OWNER=", this->getUserId(username)));
}

int SqlDataBase::getNumOfCategoryQuestions(string category)
{
	return this->core.selectInt(SQL_COMMAND("SELECT COUNT(ID) FROM TRIVIAS WHERE CATEGORY=", category));
}

std::vector<RoundData> SqlDataBase::getRandTrivias(int numberOfTrivias, string category)
{
	std::vector<RoundData> trivias; //check if there are enough questions for the category
	auto& core = this->core;
	auto command = SQL_COMMAND("SELECT ID, CATEGORY, DIFFICULTY, QUESTION FROM TRIVIAS WHERE CATEGORY LIKE", category, "ORDER BY RANDOM() LIMIT", numberOfTrivias);
	for (auto triva : core.select(command))
	{
		trivias.push_back(this->getTrivia(triva));
	}

	return trivias;
}

int SqlDataBase::getTriviaId(string question, string username)
{
	auto command = SQL_COMMAND("SELECT ID FROM TRIVIAS WHERE QUESTION=", question, "AND OWNER=", this->getUserId(username));
	auto ans = this->core.select(command);

	if (ans.empty())
	{
		return -1;
	}

	return stoi(ans[0][0]);
}

int SqlDataBase::getUserId(string username)
{
	auto commnad = SQL_COMMAND("SELECT ID FROM USERS WHERE NAME=", username);
	auto ans = this->core.select(commnad);
	if (ans.empty())
		return -1;

	return stoi(ans[0][0]);
}

std::vector<Record> SqlDataBase::getScores(string userName)
{
	auto userId = this->getUserId(userName);
	if (userId == -1) throw std::exception("can't find user");

	auto gamesCommand = SQL_COMMAND("SELECT * FROM SCORES WHERE USER_ID=", userId);
	return this->core.select(gamesCommand);
}

double SqlDataBase::getPlayerAverageAnswerTime(string userName)
{
	double totalAvarge = 0;
	int totalGames = 0;

	for (auto game : this->getScores(userName))
	{
		int scoreId = stoi(game[0]);
		auto avargeCommand = SQL_COMMAND("SELECT AVG(RESPONSE_TIME) FROM SUBMITS WHERE SCORE_ID=", scoreId);

		totalAvarge += this->core.selectFloat(avargeCommand);
		totalGames++;
	}
	if (totalGames)
		return totalAvarge / totalGames;
	else
		return 0;
}

unsigned long long SqlDataBase::getNumOfCorrectAnswers(string userName)
{
	int CorrectAnswers = 0;
	for (auto commit : this->getCommits(userName))
	{
		auto answersId = stoi(commit[2]);
		auto isCorrectCommand = SQL_COMMAND("SELECT IS_TRUE FROM ANSWERS WHERE ID=", answersId);

		if (this->core.selectInt(isCorrectCommand))
		{
			CorrectAnswers++;
		}
	}
	return CorrectAnswers;
}

Records SqlDataBase::getCommits(string userName)
{
	Records totalCommits;
	for (auto game : this->getScores(userName))
	{
		int scoreId = stoi(game[0]);
		auto gameAnssCommand = SQL_COMMAND("SELECT * FROM SUBMITS WHERE SCORE_ID=", scoreId);
		auto commuts = this->core.select(gameAnssCommand);
		totalCommits.insert(totalCommits.end(), commuts.begin(), commuts.end());  // add to the existing vector
	}
	return totalCommits;
}

unsigned long long SqlDataBase::getNumOfTotalAnswers(string userName)
{
	return this->getCommits(userName).size();
}

unsigned long long SqlDataBase::getNumOfPlayerGames(string userName)
{
	return this->getScores(userName).size();
}

bool SqlDataBase::doesPasswordMatch(string username, string password)
{
	if (!this->doesUserExist(username))
	{
		return false;
	}

	auto commnad = SQL_COMMAND("SELECT PASSWORD, SALT FROM USERS WHERE NAME=", username);
	auto passAndSalt = (this->core.selectRow(commnad));

	password += passAndSalt[1];
	return passAndSalt[0] == sha256(password);
}

void SqlDataBase::addNewUser(string username, string password, string email)
{
	string salt = this->randomSalt(9);
	string comment = SQL_COMMAND("INSERT INTO USERS(NAME, PASSWORD, EMAIL, SALT) VALUES", SQL_LIST(username, sha256(password + salt), email, salt));
	this->core.exce(comment.c_str());
}

void SqlDataBase::addNewTrivia(string question, string rightAnswer, std::vector<string> wrongAnswers, string category, string diff, string username)
{
	string addTrivia = SQL_COMMAND("INSERT INTO TRIVIAS(CATEGORY, DIFFICULTY, QUESTION, OWNER) VALUES", SQL_LIST(category, diff, question, this->getUserId(username)));
	this->core.exce(addTrivia.c_str());

	auto triviaId = this->core.lastInsertId();

	auto addRightAnswer = SQL_COMMAND("INSERT INTO ANSWERS(ANSWER, IS_TRUE, TRIVIA_ID) VALUES", SQL_LIST(rightAnswer, 1, triviaId));
	this->core.exce(addRightAnswer.c_str());

	for (auto wrongAnswer : wrongAnswers)
	{
		auto addAnswer = SQL_COMMAND("INSERT INTO ANSWERS(ANSWER, IS_TRUE, TRIVIA_ID) VALUES", SQL_LIST(wrongAnswer, 0, triviaId));
		this->core.exce(addAnswer.c_str());
	}
}

bool SqlDataBase::removeTrivia(string question, string username)
{
	auto& core = this->core;

	auto triviaId = this->getTriviaId(question, username);

	if (triviaId == -1)
	{
		return false;
	}

	auto deleteAnswer = SQL_COMMAND("DELETE FROM ANSWERS WHERE TRIVIA_ID=", triviaId);
	core.exce(deleteAnswer.c_str());

	auto deleteTrivia = SQL_COMMAND("DELETE FROM TRIVIAS WHERE ID=", triviaId);
	core.exce(deleteTrivia.c_str());

	return true;
}

int SqlDataBase::StartNewGame()
{
	this->core.exce(SQL_COMMAND("INSERT INTO GAMES DEFAULT VALUES"));
	int id = this->core.lastInsertId();
	return id;
}

int SqlDataBase::getAnswerId(int trivaID, string answer)
{
	auto getAnswerCommand = SQL_COMMAND("SELECT ID FROM ANSWERS WHERE TRIVIA_ID=", trivaID, "AND ANSWER=", answer);
	return this->core.selectInt(getAnswerCommand);
}

bool SqlDataBase::finishGame(string username, int gameId, float score, std::vector<Answer> answers)
{
	int userId = this->getUserId(username);

	// add score score userid gameid
	auto addScoreCommand = SQL_COMMAND("INSERT INTO SCORES(SCORE, USER_ID, GAME_ID) VALUES", SQL_LIST(score, userId, gameId));
	this->core.exce(addScoreCommand);
	int scoreID = this->core.lastInsertId();

	// add SUBMITS
	for (auto answer : answers)
	{
		// RESPONSE_TIME ANSWERS_ID TRIVIAS_ID SCORE_ID
		int AnswerId = this->getAnswerId(answer.trivaID, answer.answer);
		auto addScoreCommand = SQL_COMMAND("INSERT INTO SUBMITS(RESPONSE_TIME, ANSWERS_ID, TRIVIAS_ID, SCORE_ID) VALUES", SQL_LIST(answer.responseTime, AnswerId, answer.trivaID, scoreID));
		this->core.exce(addScoreCommand);
	}
	return true;
}