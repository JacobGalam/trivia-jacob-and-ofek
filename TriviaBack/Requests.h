#pragma once
#include <string>
#include "Sendible.h"
using std::string;

namespace Requests
{
	struct SignupReq
	{
		string username;
		string password;
		string email;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(SignupReq, username),
			PROPERTY(SignupReq, password),
			PROPERTY(SignupReq, email)
		);
	};

	struct LoginReq
	{
		string username;
		string password;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(LoginReq, username),
			PROPERTY(LoginReq, password)
		);
	};

	struct GetPlayersInRoomReq
	{
		string roomName;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetPlayersInRoomReq, roomName)
		);
	};

	struct JoinRoomReq
	{
		string roomName;
		string password;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(JoinRoomReq, roomName),
			PROPERTY(JoinRoomReq, password)
		);
	};

	struct ExitRoomReq
	{
		string roomName;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(ExitRoomReq, roomName)
		);
	};

	struct UserStatsReq
	{
		string userName;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(UserStatsReq, userName)
		);
	};

	struct TopReq
	{
		unsigned int maxUsers = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(TopReq, maxUsers)
		);
	};

	struct CreateRoomReq
	{
		string roomName;
		unsigned int maxUsers = 0;
		unsigned int questionCount = 0;
		unsigned int answerTimeout = 0;
		string password = "";
		string category;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(CreateRoomReq, roomName),
			PROPERTY(CreateRoomReq, maxUsers),
			PROPERTY(CreateRoomReq, questionCount),
			PROPERTY(CreateRoomReq, answerTimeout),
			PROPERTY(CreateRoomReq, password),
			PROPERTY(CreateRoomReq, category)
		);
	};

	struct CloseRoomReq
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(CloseRoomReq, status)
		);
	};

	struct StartGameReq
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(StartGameReq, status)
		);
	};

	struct GetRoomStateReq
	{
		unsigned int status = 0;
		bool hasGameBegun = 0;
		std::vector<string> players;
		unsigned questionCount = 0;
		unsigned answerAnswer = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(GetRoomStateReq, status),
			PROPERTY(GetRoomStateReq, hasGameBegun),
			PROPERTY(GetRoomStateReq, players),
			PROPERTY(GetRoomStateReq, questionCount),
			PROPERTY(GetRoomStateReq, answerAnswer)
		);
	};

	struct SubmitAnswerReq
	{
		string answer;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(SubmitAnswerReq, answer)
		);
	};

	struct GetQuestionReq // auto return when Question finish, client no need to ask
	{
		//string question;

		constexpr static auto properties = std::make_tuple(
			//PROPERTY(GetQuestionReq, question)
		);
	};

	struct LeaveGameReq
	{
		unsigned int status = 0;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(LeaveGameReq, status)
		);
	};

	struct GetGameResultReq  // auto return when game finish, client no need to ask
	{
		//GameData gameResult;

		constexpr static auto properties = std::make_tuple(
			//PROPERTY(GetGameResultReq, gameResult)
		);
	};

	struct KickUserReq //bonus
	{
		string kickedUser;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(KickUserReq, kickedUser)
		);
	};

	struct BanUserReq //bonus
	{
		string bannedUser;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(BanUserReq, bannedUser)
		);
	};

	struct SendMessageReq //bonus
	{
		string msg;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(SendMessageReq, msg)
		);
	};

	struct AddQuestionReq //bonus
	{
		string question;
		string rightAnswer;
		std::vector<string> wrongAnswers;
		string category;
		string diff;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(AddQuestionReq, question),
			PROPERTY(AddQuestionReq, rightAnswer),
			PROPERTY(AddQuestionReq, wrongAnswers),
			PROPERTY(AddQuestionReq, category),
			PROPERTY(AddQuestionReq, diff)
		);
	};

	struct RemoveQuestionReq //bonus
	{
		string questionToRemove;

		constexpr static auto properties = std::make_tuple(
			PROPERTY(RemoveQuestionReq, questionToRemove)
		);
	};
}