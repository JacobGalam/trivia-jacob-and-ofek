#include "TriviaManager.h"

TriviaManager::TriviaManager(IDatabase* iDataBase)
{
	this->m_database = iDataBase;
}

vector<string> TriviaManager::getUserQuestions(string username)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	return this->m_database->getUserQuestions(username);
}

vector<string> TriviaManager::getCategories()
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	return this->m_database->getCategories();;
}

void TriviaManager::removeTrivia(string question, string username)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	// two oarts for know if the Trivia is of the player and if it exist
	if (!this->m_database->doesTriviaExist(question, username))
	{
		throw std::exception("That question does not exist!");
	}
	else if (!this->m_database->removeTrivia(question, username))
	{
		throw std::exception("That is not your question!");
	}
}

void TriviaManager::addTrivia(string question, string rightAnswer, std::vector<string> wrongAnswers, string category, string diff, string username)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	if (this->m_database->doesTriviaExist(question, username))
	{
		throw std::exception("That question already exists!");
	}
	this->m_database->addNewTrivia(question, rightAnswer, wrongAnswers, category, diff, username);
}

int TriviaManager::getNumOfCategoryQuestions(string category)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	return this->m_database->getNumOfCategoryQuestions(category);
}