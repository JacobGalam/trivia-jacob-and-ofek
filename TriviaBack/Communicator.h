#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <map>
#include <streambuf>
#include <ws2tcpip.h>
#include <memory>
#include <thread>
#include <fstream>

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Helper.h"
#include "WSAInitializer.h"
#include "SqlDataBase.h"
#include "Socket.h"
#include "RequestResult.h"
#include "LoginManager.h"
#include "TimeOutException.h"

using std::map;
using std::thread;
using std::unique_ptr;

#define MIN 60
#define TIMEOUT 10 * MIN

// the server config
typedef struct ServerConfig
{
	string ip;
	int port = -1;
} ServerConfig;

class Communicator
{
public:
	static Communicator& getInstance();
public:
	template<class responseType>
	bool static sendResponse(LoggedUser user, responseType data)
	{
		Socket* userSock = LoginManager::usersSock[user];
		try
		{
			Buffer sendData = JsonResponsePacketSerializer::serializeResponse(data);
			userSock->send(sendData);
		}
		catch (const std::exception&)
		{
			Communicator::getInstance().disconnectSocket(userSock);
			return false;
		}

		return true;
	}

	template<class responseType>
	bool static sendResponses(std::vector<LoggedUser> users, responseType data, LoggedUser except)
	{
		bool isFine = true;
		for (auto user : users)
		{
			if (user.getUsername() != except.getUsername())
			{
				if (!sendResponse(user, data)) isFine = false;
			}
		}
		return isFine;
	}

	void changeHandle(LoggedUser user, IRequestHandler* newHandle)
	{
		Socket* userSock = LoginManager::usersSock[user];
		delete this->m_clients[userSock->sock];
		this->m_clients[userSock->sock] = newHandle;
	}
	/*
	Config the server
	Input: The config
	Output: None
	*/
	/*
	deleting all the clients
	Input: None
	Output: None
	*/
	~Communicator();
	/*
	Start handling requests
	Input: None
	Output: None
	*/
	void startHandleRequests();

	RequestHandlerFactory& getFacotory();

private:
	Communicator();
	/*
	TODO Json
	Read the ip and port form the config file
	Input: Config name
	Output: Struct of ip and port
	*/
	static ServerConfig readConfig(string fileName);

	void disconnectSocket(Socket* sock);

	/*
	Start the new connection
	Input: None
	Output: None
	*/
	void bindAndListen();

	/*
	Listening for clients and staring new threads for them
	Input: The socket and this class ref
	Output: None
	*/
	void getClients();
	/*
	Server logic run in thread
	Input: The socket and this class ref
	Output: None
	*/
	void handleNewClient(SOCKET clientSocket);

public:
	map<SOCKET, IRequestHandler*> m_clients;
private:
	IDatabase* db;
	RequestHandlerFactory requestHandlerFactory;
	ServerConfig m_serverData;
	SOCKET m_listenSocket = INVALID_SOCKET;
};
