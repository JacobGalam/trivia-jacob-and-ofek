#pragma once
#include <string>
#include <fstream>
#include <set>
#include <mutex>
#include <streambuf>
#include <memory>
#include <exception>
#include <iostream>
#include <string>
#include "Helper.h"
#include "SqlDataBase.h"
#include "Communicator.h"
#include "WSAInitializer.h"

using std::string;
using std::thread;
using std::set;
using std::mutex;
using std::cout;
using std::unique_ptr;

class Server
{
public:
	/*
	Start the server
	Input: None
	Output: None
	*/
	Server();
	/*
	Deleting the db
	Input: None
	Output: None
	*/
	~Server();
	/*
	Start the server
	Input: None
	Output: None
	*/
	void run();

private:
	Communicator& m_communicator;
};
