#pragma once
#include "InputCheck.h"
#include "RequestHandlerFactory.h"
#include "IRequestHandler.h"

namespace MenuAndRoomOptions
{
	enum Codes
	{
		getUserStatistics = 4
	};
}

/*
4 - getUserStatistics
*/
class MenuRoomHandler : IRequestHandler
{
public:
	MenuRoomHandler(RequestHandlerFactory& factory);
	// Inherited via IRequestHandler
	virtual handleRequestFunc_map& optionsMap() override;

	RequestResult static getUserStatistics(IRequestHandler* self, RequestInfo rInfo);

private:
	RequestHandlerFactory& factory;
};
