#include "RoomMemberRequestHandler.h"

using namespace RoomMemberOptions;

RoomMemberRequestHandler::RoomMemberRequestHandler(string roomName, LoggedUser user, RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory)
	:
	DefaultMemberRequstHandler(roomName, user, roomManager, requestHandlerFactory)
{
}

handleRequestFunc_map& RoomMemberRequestHandler::optionsMap()
{
	static handleRequestFunc_map returnMap = DefaultMemberRequstHandler::optionsMap();
	static bool initMyFunctions = false;

	if (!initMyFunctions)
	{
		returnMap[Codes::leaveRoom] = &RoomMemberRequestHandler::leaveRoom;
		initMyFunctions = true;
	}

	return returnMap;
}

void RoomMemberRequestHandler::onExit()
{
	Responses::UserLeave userLeave;
	userLeave.username = this->m_user.getUsername();

	auto& room = this->m_roomManager.getRoomState(this->roomName);

	room.removeUser(this->m_user);

	for (auto LoggedUser : room.getAllUsers())
	{
		Communicator::getInstance().sendResponse(LoggedUser, userLeave);
	}
}

RequestResult RoomMemberRequestHandler::leaveRoom(IRequestHandler* self, RequestInfo rInfo)
{
	RoomMemberRequestHandler* mySelf = (RoomMemberRequestHandler*)self;

	RequestResult rR;

	Responses::ExitRoom exitRoom;

	mySelf->onExit();

	rR.response = JsonResponsePacketSerializer::serializeResponse(exitRoom);
	rR.newHandler = mySelf->m_handlerFactory.createMenuRequestHandler(mySelf->m_user.getUsername());
	return rR;
}