#include "MenuRoomHandler.h"

using namespace MenuAndRoomOptions;

MenuRoomHandler::MenuRoomHandler(RequestHandlerFactory& factory) : factory(factory)
{
}

handleRequestFunc_map& MenuRoomHandler::optionsMap()
{
	static handleRequestFunc_map returnMap =
	{
		{Codes::getUserStatistics, &MenuRoomHandler::getUserStatistics},
	};

	return returnMap;
}

RequestResult MenuRoomHandler::getUserStatistics(IRequestHandler* self, RequestInfo rInfo)
{
	MenuRoomHandler* mySelf = (MenuRoomHandler*)self;

	RequestResult rR;

	auto userStatsR = JsonRequestPacketDeserializer::deserializeUserStatsReqRequest(rInfo.buffer);

	InputCheck::MinMax(userStatsR.userName, 3, 20, "wanted username");

	Responses::UserStats userStats = mySelf->factory.getStatisticsManager().getUserStatistics(userStatsR.userName);
	userStats.userName = userStatsR.userName;
	rR.response = JsonResponsePacketSerializer::serializeResponse(userStats);
	return rR;
}