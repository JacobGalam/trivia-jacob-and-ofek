#include "LoginManager.h"

std::map<LoggedUser, Socket*> LoginManager::usersSock;

bool LoginManager::isEmailValid(const std::string& email)
{
	// define a regular expression
	const std::regex pattern
	("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");

	// try to match the string with the regular expression
	return std::regex_match(email, pattern);
}

bool LoginManager::isPasswordStrong(const std::string& password)
{
	// define a regular expression
	int n = password.length();

	// Checking lower alphabet in string
	bool hasLower = false, hasUpper = false;
	bool hasDigit = false, specialChar = false;
	string normalChars = "abcdefghijklmnopqrstu"
		"vwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ";

	if (n < 8)
		return false;

	for (int i = 0; i < n; i++) {
		if (islower(password[i]))
			hasLower = true;
		if (isupper(password[i]))
			hasUpper = true;
		if (isdigit(password[i]))
			hasDigit = true;

		size_t special = password.find_first_not_of(normalChars);
		if (special != string::npos)
			specialChar = true;

		if (hasLower && hasUpper && hasDigit && specialChar)
			return true;
	}

	return false;
}

LoginManager::LoginManager(IDatabase* db)
{
	this->m_database = db;
}

void LoginManager::newLog(string username, string password, Socket* sock)
{
	auto& logins = this->m_loggedUsers;

	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	if (logins.find(username) != logins.end())
	{
		throw std::exception("this username already used");
	}

	if (!this->m_database->doesPasswordMatch(username, password))
	{
		throw std::exception("wrong password or username");
	}
	addUser(username, sock);
}

bool LoginManager::newSignup(string username, string password, string email, Socket* sock)
{
	//vector<string> a = { username, password, email };
	//string a[] = { username, password, email };

	if (!this->isEmailValid(email))
	{
		throw std::exception("E-mail is invalid");
	}

	if (!this->isPasswordStrong(password))
	{
		throw std::exception("password must have at least 1 lowercase and uppercase alphabetical character, numeric character and special character");
	}

	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	if (this->m_database->doesUserExist(username))
	{
		throw std::exception("not have user with this username");
	}

	this->m_database->addNewUser(username, password, email);
	addUser(username, sock);
	return true;
}

bool LoginManager::logoutUser(string username)
{
	auto& logins = this->m_loggedUsers;
	auto user = logins.find(username);
	auto a = user->second;
	if (user == logins.end())
	{
		return false;
	}

	logins.erase(username);

	return true;
}

void LoginManager::addUser(string username, Socket* sock)
{
	auto newUser = LoggedUser(username);
	this->m_loggedUsers[username] = newUser;
	LoginManager::usersSock[newUser] = sock;
}