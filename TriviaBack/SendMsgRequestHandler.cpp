#include "SendMsgRequestHandler.h"

using OptionCode::SendMsgOption;

SendMsgRequestHandler::SendMsgRequestHandler(SendDataMsgFunc* sendDataFunc, LoggedUser& user)
	:
	sendDataFunc(sendDataFunc), user(user)
{
}

handleRequestFunc_map& SendMsgRequestHandler::optionsMap()
{
	static handleRequestFunc_map returnMap =
	{
		{SendMsgOption::sendMessage, &SendMsgRequestHandler::sendMessage},
	};

	return returnMap;
}

RequestResult SendMsgRequestHandler::sendMessage(IRequestHandler* self, RequestInfo rInfo)
{
	SendMsgRequestHandler* mySelf = (SendMsgRequestHandler*)self;

	RequestResult rR;

	auto msgData = JsonRequestPacketDeserializer::deserializeSendMessageReqRequest(rInfo.buffer);

	bool whiteSpacesOnly = std::all_of(msgData.msg.begin(), msgData.msg.end(), isspace);
	if (whiteSpacesOnly)
	{
		throw std::exception("Message can't be empty");
	}

	InputCheck::NotZeroAndMax(msgData.msg, 50, "Message");

	Responses::SendMessageChat sendMsg;

	sendMsg.user = mySelf->user.getUsername();
	sendMsg.msg = msgData.msg;

	mySelf->sendDataFunc(mySelf, sendMsg, mySelf->user);

	rR.response = JsonResponsePacketSerializer::serializeResponse(sendMsg);
	return rR;
}

void SendMsgRequestHandler::onExit()
{
}