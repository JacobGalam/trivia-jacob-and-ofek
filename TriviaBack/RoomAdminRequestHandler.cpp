#include "RoomAdminRequestHandler.h"

using namespace RoomAdminOptions;

RoomAdminRequestHandler::RoomAdminRequestHandler(string roomName, LoggedUser user,
	RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory)
	:
	DefaultMemberRequstHandler(roomName, user, roomManager, requestHandlerFactory)
{
}

handleRequestFunc_map& RoomAdminRequestHandler::optionsMap()
{
	static handleRequestFunc_map returnMap = DefaultMemberRequstHandler::optionsMap();
	static bool initMyFunctions = false;

	if (!initMyFunctions)
	{
		returnMap[Codes::closeRoom] = &RoomAdminRequestHandler::closeRoom;
		returnMap[Codes::startGame] = &RoomAdminRequestHandler::startGame;
		returnMap[Codes::kickUser] = &RoomAdminRequestHandler::kickUser;
		returnMap[Codes::banUser] = &RoomAdminRequestHandler::banUser;
		initMyFunctions = true;
	}

	return returnMap;
}

void RoomAdminRequestHandler::onExit()
{
	Responses::CloseRoom clintMsg;
	auto& room = this->m_roomManager.getRoomState(this->roomName);
	room.removeUser(this->m_user);
	for (LoggedUser user : room.getAllUsers())
	{
		Communicator::getInstance().sendResponse(user, clintMsg);
	}

	this->m_roomManager.deleteRoom(this->roomName);
}

RequestResult RoomAdminRequestHandler::closeRoom(IRequestHandler* self, RequestInfo rInfo)
{
	RoomAdminRequestHandler* mySelf = (RoomAdminRequestHandler*)self;

	RequestResult rR;

	Responses::ExitRoom adminMsg;
	mySelf->onExit();

	rR.response = JsonResponsePacketSerializer::serializeResponse(adminMsg);
	rR.newHandler = mySelf->m_handlerFactory.createMenuRequestHandler(mySelf->m_user.getUsername());
	return rR;
}

RequestResult RoomAdminRequestHandler::startGame(IRequestHandler* self, RequestInfo rInfo)
{
	RoomAdminRequestHandler* mySelf = (RoomAdminRequestHandler*)self;

	RequestResult rR;
	Responses::StartGamedata startRoomData;

	auto& roomManager = mySelf->m_roomManager;

	auto& room = mySelf->m_roomManager.getRoomState(mySelf->roomName);

	startRoomData.gameName = room.m_metadata.roomName;
	startRoomData.questionNum = room.m_metadata.questionCount;
	startRoomData.timePerquestion = room.m_metadata.answerTimeout;
	for (auto user : room.getAllUsers())
	{
		startRoomData.players.push_back(user.getUsername());
	}
	auto gameId = mySelf->m_handlerFactory.getGameManager().createGame(room);

	auto& game = mySelf->m_handlerFactory.getGameManager().getGame(gameId);
	auto& factory = mySelf->m_handlerFactory;
	for (LoggedUser user : room.getAllUsers())
	{
		Communicator::getInstance().sendResponse(user, startRoomData);
		Communicator::getInstance().changeHandle(user, factory.createGameRequestHandler(gameId, user));
	}

	roomManager.deleteRoom(room.m_metadata.roomName);
	game.startGame();
	rR.response = JsonResponsePacketSerializer::serializeResponse(Responses::StartGame());

	return rR;
}

RequestResult RoomAdminRequestHandler::kickUser(IRequestHandler* self, RequestInfo rInfo)
{
	RoomAdminRequestHandler* mySelf = (RoomAdminRequestHandler*)self;

	RequestResult rR;

	auto roomData = JsonRequestPacketDeserializer::deserializeKickUserReqRequest(rInfo.buffer);

	Responses::KickUser kickUser;
	Responses::UserLeave userLeave;
	userLeave.username = roomData.kickedUser;
	LoggedUser kickedUser;

	for (LoggedUser user : mySelf->m_roomManager.getRoomState(mySelf->roomName).getAllUsers())
	{
		if (user.getUsername() == roomData.kickedUser)
		{
			kickedUser = user;
			break;
		}
	}

	if (kickedUser.getUsername().empty()) throw std::exception("user not in the room");

	auto& room = mySelf->m_roomManager.getRoomState(mySelf->roomName);

	auto& comm = Communicator::getInstance();
	comm.sendResponse(kickedUser, kickUser);
	comm.changeHandle(kickedUser, comm.getFacotory().createMenuRequestHandler(kickedUser.getUsername()));
	kickUser.status = room.removeUser(kickedUser);

	comm.sendResponses(room.getAllUsers(), userLeave, mySelf->m_user);

	rR.response = JsonResponsePacketSerializer::serializeResponse(userLeave);
	return rR;
}

RequestResult RoomAdminRequestHandler::banUser(IRequestHandler* self, RequestInfo rInfo)
{
	RoomAdminRequestHandler* mySelf = (RoomAdminRequestHandler*)self;

	RequestResult rR;

	auto roomData = JsonRequestPacketDeserializer::deserializeBanUserReqRequest(rInfo.buffer);

	Responses::BanUser banUser;
	Responses::UserLeave userLeave;
	userLeave.username = roomData.bannedUser;
	LoggedUser bannedUser;

	for (LoggedUser user : mySelf->m_roomManager.getRoomState(mySelf->roomName).getAllUsers())
	{
		if (user.getUsername() == roomData.bannedUser)
		{
			bannedUser = user;
			break;
		}
	}

	if (bannedUser.getUsername().empty()) throw std::exception("user not in the room");

	auto& room = mySelf->m_roomManager.getRoomState(mySelf->roomName);
	room.m_blackListUsers.insert(bannedUser);

	auto& comm = Communicator::getInstance();
	comm.sendResponse(bannedUser, banUser);
	comm.changeHandle(bannedUser, comm.getFacotory().createMenuRequestHandler(bannedUser.getUsername()));

	comm.sendResponses(room.getAllUsers(), userLeave, mySelf->m_user);

	rR.response = JsonResponsePacketSerializer::serializeResponse(userLeave);
	return rR;
}