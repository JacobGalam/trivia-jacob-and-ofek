#include "GameRequestHandler.h"

using namespace GameOptions;

GameRequestHandler::GameRequestHandler(Game& game, LoggedUser user, GameManager& gameManager, RequestHandlerFactory& handlerFactory) : m_game(game), m_user(user), m_gameManager(gameManager), m_handlerFactory(handlerFactory), SendMsgRequestHandler(&GameRequestHandler::sendGameMessage, m_user)
{
}

RequestResult GameRequestHandler::submitAnswer(IRequestHandler* self, RequestInfo rInfo)
{
	GameRequestHandler* mySelf = (GameRequestHandler*)self;
	RequestResult rR;

	auto response = JsonRequestPacketDeserializer::deserializeSubmitAnswerReqRequest(rInfo.buffer);

	InputCheck::NotZeroAndMax(response.answer, 100, "answer");
	mySelf->m_game.submitAnswer(mySelf->m_user, response.answer, rInfo.receivalTime);

	Responses::SubmitAnswer returnAns;
	returnAns.status = 1;

	rR.response = JsonResponsePacketSerializer::serializeResponse(returnAns);
	return rR;
}

RequestResult GameRequestHandler::leaveGame(IRequestHandler* self, RequestInfo rInfo)
{
	GameRequestHandler* mySelf = (GameRequestHandler*)self;
	RequestResult rR;

	mySelf->onExit();

	Responses::LeaveGame returnAns;
	returnAns.status = 1;

	rR.newHandler = mySelf->m_handlerFactory.createMenuRequestHandler(mySelf->m_user.getUsername());
	rR.response = JsonResponsePacketSerializer::serializeResponse(returnAns);
	return rR;
}

void GameRequestHandler::sendGameMessage(IRequestHandler* self, Responses::SendMessageChat msg, LoggedUser sender)
{
	GameRequestHandler* mySelf = (GameRequestHandler*)self;
	mySelf->sendMsgToUsers(msg, sender);
}

void GameRequestHandler::onExit()
{
	this->m_game.removePlayer(this->m_user);
}

handleRequestFunc_map& GameRequestHandler::optionsMap()
{
	static handleRequestFunc_map returnMap = SendMsgRequestHandler::optionsMap();
	static bool initMyFunction = false;

	if (!initMyFunction)
	{
		returnMap[Codes::submitAnswer] = &GameRequestHandler::submitAnswer;
		returnMap[Codes::leaveGame] = &GameRequestHandler::leaveGame;
		initMyFunction = true;
	}

	return returnMap;
}