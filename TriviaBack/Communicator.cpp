#include "Communicator.h"

Communicator::Communicator() : db(new SqlDataBase()), requestHandlerFactory(db)
{
	this->m_serverData = Communicator::readConfig("config.txt");
	if (this->m_serverData.port == -1)  // if cant read
	{
		throw ("cant read the config.txt file");
	}
}

Communicator& Communicator::getInstance()
{
	static Communicator    instance; // Guaranteed to be destroyed.
							  // Instantiated on first use.
	return instance;
}

Communicator::~Communicator()
{
	delete db;
	for (const auto& [socket, handle] : this->m_clients) {
		delete handle;
	}

	try
	{
		// the only use of the destructor should be for freeing
		// resources that was allocated in the constructor
		closesocket(this->m_listenSocket);
	}
	catch (...) {}
}

void Communicator::startHandleRequests()
{
	this->bindAndListen();
	thread newClient = thread(&Communicator::getClients, this);
	newClient.detach();
}

ServerConfig Communicator::readConfig(string fileName)
{
	ServerConfig configDataReturn;

	std::ifstream configFile(fileName);

	if (configFile.is_open())
	{
		char ipLine[26] = { 0 }; // 25 is the max size of ip
		configFile.getline(ipLine, 26);
		configDataReturn.ip = string(ipLine).erase(0, 10);  // remove: server_ip=

		char portLine[11] = { 0 }; // 11 is the max size of port
		configFile.getline(portLine, 11);
		if (portLine)
		{
			string portLineStr = string(portLine).erase(0, 5);  // remove: port=
			configDataReturn.port = std::stoi(portLineStr);
		}
	}

	return configDataReturn;
}

void Communicator::disconnectSocket(Socket* sock)
{
	auto client = this->m_clients.find(sock->sock);
	client->second->onExit();
	delete client->second;
	this->m_clients.erase(client);
}

void Communicator::bindAndListen()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	WSADATA wsaData;
	int iResult;

	struct addrinfo* result = NULL;
	struct addrinfo hints;
	#define DEFAULT_BUFLEN 512

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		throw std::exception(__FUNCTION__);;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, std::to_string(m_serverData.port).c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		throw std::exception(__FUNCTION__);;
	}

	// Create a SOCKET for connecting to server
	m_listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	DWORD timeout = TIMEOUT * 1000;

	if (setsockopt(m_listenSocket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout) < 0)
		throw std::exception(__FUNCTION__);

	if (setsockopt(m_listenSocket, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof timeout) < 0)
		throw std::exception(__FUNCTION__);

	if (m_listenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		throw std::exception(__FUNCTION__);;
	}
	std::cout << "start server on port " << m_serverData.port << "\n";
	// Setup the TCP listening socket
	iResult = bind(m_listenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(m_listenSocket);
		WSACleanup();
		throw std::exception(__FUNCTION__);;
	}
	std::cout << "binding...\n";
	freeaddrinfo(result);

	iResult = listen(m_listenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(m_listenSocket);
		WSACleanup();
		throw std::exception(__FUNCTION__);
	}
}

RequestHandlerFactory& Communicator::getFacotory()
{
	return this->requestHandlerFactory;
}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	RequestInfo requestInfo;
	Socket sock = Socket(clientSocket);
	requestInfo.userSock = &sock;
	auto client = this->m_clients.find(clientSocket);
	while (true)
	{
		try
		{
			requestInfo.buffer = sock.receive();
			time(&requestInfo.receivalTime);  // update the time to now

			RequestResult requestResult = client->second->handleRequest(requestInfo);

			if (requestResult.newHandler != nullptr)
			{
				// update the now handle
				delete client->second;
				client->second = requestResult.newHandler;
			}

			sock.send(requestResult.response);
		}
		catch (const TimeOutException& e)
		{
			if (!e.isSend)
			{
				Responses::Error err;
				err.close = true;
				err.message = "timeout disconnected";
				try
				{
					sock.send(JsonResponsePacketSerializer::serializeResponse(err));
				}
				catch (const std::exception&) {}
			}
			this->disconnectSocket(&sock);
			return;
		}
		catch (const std::exception& e)
		{
			Responses::Error err;
			err.message = std::string(e.what());

			try
			{
				sock.send(JsonResponsePacketSerializer::serializeResponse(err));
			}
			catch (const std::exception&)
			{
				this->disconnectSocket(&sock);
				return;
			}
		}
	}
}

void Communicator::getClients()
{
	while (true)
	{
		std::cout << "start listen\n";
		SOCKET ClientSocket = ::accept(this->m_listenSocket, NULL, NULL);
		if (ClientSocket == INVALID_SOCKET) {
			printf("accept failed with error: %d\n", WSAGetLastError());
			closesocket(this->m_listenSocket);
			WSACleanup();
			throw std::exception(__FUNCTION__);
		}
		std::cout << "get accept!\n";
		this->m_clients[ClientSocket] =
			this->requestHandlerFactory.createLoginRequestHandler();
		// start new thread
		thread newClient = thread(&Communicator::handleNewClient, this, ClientSocket);
		newClient.detach();
	}
}