#include "Game.h"
#include "Communicator.h"

Game::Game(Room& room, std::vector<RoundData> rounds, unsigned int gameId) : m_currentQuestion(0), m_canGetAnswers(false), gameId(gameId)
{
	this->m_metadata = room.m_metadata;
	this->m_questions = rounds;

	for (LoggedUser user : room.getAllUsers())
	{
		this->m_players[user] = UserGameStats();
	}
}

bool Game::submitAnswer(LoggedUser user, string answer, time_t receivalTime)
{
	if (!this->m_canGetAnswers) return false;

	if (this->m_answeredThisRound.find(user) != this->m_answeredThisRound.end()) return false;

	this->m_answeredThisRound.insert(user);

	auto& playerData = this->m_players[user];

	auto roundData = this->m_questions[this->m_currentQuestion];
	auto questionDif = roundData.difficulty;
	auto diff = receivalTime - this->m_receivalTime;
	float percentTimeLeft = diff / this->m_metadata.answerTimeout; //answered fast - limited to 1, answered slow - limited to 0
	int scorePerQuestion = 0;

	Answer ansData;
	ansData.answer = answer;
	ansData.responseTime = diff;
	ansData.trivaID = this->m_questions[this->m_currentQuestion].id;
	this->allAnswers[user].push_back(ansData);

	if (answer == roundData.correctAnswer)
	{
		switch (questionDif[0])
		{
		case 'e':
			scorePerQuestion = 1;
			break;
		case 'm':
			scorePerQuestion = 2;
			break;
		case 'h':
			scorePerQuestion = 4;
			break;
		}

		playerData.correctAnswerCount++;
	}
	else
	{
		playerData.wrongAnswerCount++;
	}

	playerData.averageAnswerTime += diff;

	auto roundScore = (1 - percentTimeLeft) * scorePerQuestion;
	playerData.score += roundScore;
}

bool Game::removePlayer(LoggedUser user)
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	this->m_players.erase(user);
	return true;
}

void Game::startHandlingGame()
{
	std::this_thread::sleep_for(std::chrono::seconds(2));  // wait for the clients to start the game

	for (this->m_currentQuestion = 0; this->m_currentQuestion < this->m_metadata.questionCount && !this->getPlayers().empty(); this->m_currentQuestion++)
	{
		{
			std::lock_guard<std::mutex> lck(this->m_managerMutex);

			this->m_answeredThisRound.clear();
			time(&this->m_receivalTime);

			this->m_canGetAnswers = true;
			sendQuestion();
		}
		std::this_thread::sleep_for(std::chrono::seconds(this->m_metadata.answerTimeout));

		{
			std::lock_guard<std::mutex> lck(this->m_managerMutex);
			this->m_canGetAnswers = false;
		}

		std::this_thread::sleep_for(std::chrono::seconds(5));

		{
			std::lock_guard<std::mutex> lck(this->m_managerMutex);
			revealCorrectAnswer();
		}

		std::this_thread::sleep_for(std::chrono::seconds(this->m_metadata.answerTimeout / 2));
	}

	sendGameOver();
}

void Game::sendQuestion()
{
	Responses::GetQuestion getQuestion;
	auto& roundData = this->m_questions[this->m_currentQuestion];

	getQuestion.question = roundData.question;
	getQuestion.answers = roundData.incorrectAnswers;
	getQuestion.answers.push_back(roundData.correctAnswer);

	for (auto const& [player, _] : this->m_players)
	{
		auto rd = std::random_device{};
		auto rng = std::default_random_engine{ rd() };

		std::shuffle(std::begin(getQuestion.answers), std::end(getQuestion.answers), rng);

		Communicator::getInstance().sendResponse(player, getQuestion);
	}
}

void Game::revealCorrectAnswer()
{
	Responses::updateRightAnswer sentAnswer;
	sentAnswer.rightAnswer = this->m_questions[this->m_currentQuestion].correctAnswer;

	for (auto const& [player, userScore] : this->m_players)
	{
		Responses::Score userStats;
		userStats.score = userScore.score;
		userStats.username = player.getUsername();
		sentAnswer.gameScores.push_back(userStats);
	}
	for (auto const& [player, score] : this->m_players)
	{
		sentAnswer.score = score.score;
		Communicator::getInstance().sendResponse(player, sentAnswer);
	}
}

void Game::sendGameOver()
{
	Responses::GetGameResults sendResults;

	sendResults.status = 1;

	for (auto const& [player, userScore] : this->m_players)
	{
		Responses::PlayerResults pResults;

		pResults.username = player.getUsername();
		pResults.averageAnswerTime = userScore.averageAnswerTime / this->m_currentQuestion;
		pResults.correctAnswerCount = userScore.correctAnswerCount;
		pResults.wrongAnswerCount = userScore.wrongAnswerCount;
		pResults.finalResult = userScore.score;

		sendResults.results.push_back(pResults);
	}

	for (auto const& [player, userScore] : this->m_players)
	{
		auto& comm = Communicator::getInstance();
		comm.sendResponse(player, sendResults);
		comm.changeHandle(player, comm.getFacotory().createMenuRequestHandler(player.getUsername()));
		comm.getFacotory().getStatisticsManager().postStatistics(player.getUsername(), this->gameId, userScore.score, this->allAnswers[player]);
	}
}

void Game::startGame()
{
	std::thread t(&Game::startHandlingGame, this);

	t.detach();
}

unsigned int Game::getGameId()
{
	return this->gameId;
}

std::map<LoggedUser, UserGameStats> Game::getPlayers()
{
	std::lock_guard<std::mutex> lck(this->m_managerMutex);
	return this->m_players;
}