#include "LoginRequestHandler.h"

using namespace loginOptions;

handleRequestFunc_map& LoginRequestHandler::optionsMap()
{
	static handleRequestFunc_map returnMap =
	{
		{Codes::login,  &LoginRequestHandler::login},
		{Codes::signup,  &LoginRequestHandler::signup},
	};

	return returnMap;
}

LoginRequestHandler::LoginRequestHandler(LoginManager& lManager, RequestHandlerFactory* requestHandlerF) : m_loginManager(lManager), m_handlerFactory(requestHandlerF)
{
}

RequestResult LoginRequestHandler::login(IRequestHandler* self, RequestInfo what)
{
	LoginRequestHandler* mySelf = (LoginRequestHandler*)self;
	RequestResult rR;

	auto loginData = JsonRequestPacketDeserializer::deserializeLoginReqRequest(what.buffer);

	InputCheck::NotZeroAndMax(loginData.password, 40, "password");
	InputCheck::MinMax(loginData.username, 3, 20, "username");

	mySelf->m_loginManager.newLog(loginData.username, loginData.password, what.userSock);

	Responses::Login returnAns;
	returnAns.status = 1;

	rR.response = JsonResponsePacketSerializer::serializeResponse(returnAns);
	if (returnAns.status)
		rR.newHandler = mySelf->m_handlerFactory->createMenuRequestHandler(loginData.username); // go to the menu after login
	return rR;
}

RequestResult LoginRequestHandler::signup(IRequestHandler* self, RequestInfo what)
{
	LoginRequestHandler* mySelf = (LoginRequestHandler*)self;
	RequestResult rR;

	auto response = JsonRequestPacketDeserializer::deserializeSignupReqRequest(what.buffer);
	bool ok = mySelf->m_loginManager.newSignup(response.username, response.password, response.email, what.userSock);

	Responses::Signup returnAns;
	returnAns.status = ok;

	rR.response = JsonResponsePacketSerializer::serializeResponse(returnAns);
	if (returnAns.status)
		rR.newHandler = mySelf->m_handlerFactory->createMenuRequestHandler(response.username); // go to the menu after login
	return rR;
}